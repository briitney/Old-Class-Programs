/*
 * main.h
 *
 * Created on: Feb 11, 2017
 *     Author: Britney Reese
 *      Class: CS-222-02-17w
 */

#ifndef MAIN_H_
#define MAIN_H_

#include <iostream>
#include <string>
#include <cstdlib>
#include "LinkedList.h"

using namespace std;

class LinkedList;
void getInputIntoList(LinkedList* list, string temp);

#endif /* MAIN_H_ */
