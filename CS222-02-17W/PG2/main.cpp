/*
 * main.cpp
 *
 * Created on: Feb 11, 2017
 *     Author: Britney Reese
 *      Class: CS-222-02-17w
 */

#include <iostream>
#include <string>
#include <cstdlib>
#include "main.h"
#include "LinkedList.h"

using namespace std;

//Just make a linked list and run it's methods.
int main (int argv, char** arvc) {
	cout << "Enter strings delimited by a new line, where entering an empty string executes the sort: " << endl;
	string temp;
	LinkedList* list = new LinkedList();
	getInputIntoList(list, temp);
	list->selectionSort();
	list->print();
	delete list;
	return 0;
}

//Get input recursively because everything else is recursive, so why not?
void getInputIntoList(LinkedList* list, string temp) {
	getline(cin, temp);
	if (temp == "") return;
	else {
		list->add(temp);
		getInputIntoList(list, temp);
	}
}
