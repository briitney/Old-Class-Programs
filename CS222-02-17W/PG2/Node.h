/*
 * Node.h
 *
 * Created on: Feb 11, 2017
 *     Author: Britney Reese
 *      Class: CS-222-02-17w
 */

#ifndef NODE_H_
#define NODE_H_

#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

class Node{
private:
	string info;
	Node* next;

public:
	Node(string a, Node* next);
	void print();
	Node* selectionSort();
	Node* findLargest();
	void extract(Node * &prevnext, Node* a);
	string getInfo();
	void setNext(Node* a);
	~Node();

};

#endif /* NODE_H_ */
