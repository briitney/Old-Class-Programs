/*
 * LinkedList.cpp
 *
 * Created on: Feb 11, 2017
 *     Author: Britney Reese
 *      Class: CS-222-02-17w
 */

#include <iostream>
#include <string>
#include <cstdlib>
#include "LinkedList.h"

using namespace std;

LinkedList::LinkedList() {
	head = NULL;
}

void LinkedList::add(string a) {
	head = new Node(a, head);
}

void LinkedList::selectionSort () {
	if (head) head = head->selectionSort();
}

void LinkedList::print() {
	if (head) head->print();
}

LinkedList::~LinkedList() {
	delete head;
}
