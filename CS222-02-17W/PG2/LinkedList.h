/*
 * LinkedList.h
 *
 * Created on: Feb 11, 2017
 *     Author: Britney Reese
 *      Class: CS-222-02-17w
 */

//Standard LinkedList, nothing to see here.

//Selection Sort is just passes off to the head if the head exists.

#ifndef LINKEDLIST_H_
#define LINKEDLIST_H_

#include <iostream>
#include <string>
#include <cstdlib>
#include "Node.h"

using namespace std;

class Node;
class LinkedList {
private:
	Node* head;

public:
	LinkedList();
	void add(string a);
	void selectionSort();
	void print();
	~LinkedList();
};

#endif /* LINKEDLIST_H_ */
