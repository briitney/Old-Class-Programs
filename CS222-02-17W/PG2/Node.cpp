/*
 * Node.cpp
 *
 * Created on: Feb 11, 2017
 *     Author: Britney Reese
 *      Class: CS-222-02-17w
 */

#include <iostream>
#include <string>
#include <cstdlib>
#include "Node.h"

using namespace std;

Node::Node(string a, Node* next) {
	info = a;
	this->next = next;
}

void Node::print() {
	cout << info << endl ;
	if (next) next->print();
}

//First, go through the list after yourself and see what the largest node is.
//If you're larger than that guy, go ahead and sort the rest of the list after yourself,
//because you know you're the largest node in the list beginning with you.
//Otherwise, make him the head of the list (up to your point) and sort the remaining list,
//including yourself.
Node* Node::selectionSort() {
	if (!next) return this;
	Node* sublargest = next->findLargest();
	if (info <= sublargest->getInfo()) {
		next = next->selectionSort();
		return this;
	} else {
		next->extract(next, sublargest);
		sublargest->setNext(this->selectionSort());
		return sublargest;
	}
}

//Find and return a point to the largest node in the list
Node* Node::findLargest() {
	if (!next) return this;
	Node* cur = next->findLargest();
	if (info <= cur->getInfo()) {
		return this;
	} else return cur;
}

//Remove the first occurrence of the node from the list without destroying the node.
void Node::extract(Node * &prevnext, Node* a) {
	if (info == a->getInfo()) {
		prevnext = next;
		next = NULL;
	} else {
		next->extract(next, a);
	}
}

string Node::getInfo() {
	return info;
}

void Node::setNext(Node* a) {
	next = a;
}

Node::~Node() {
	delete next;
}
