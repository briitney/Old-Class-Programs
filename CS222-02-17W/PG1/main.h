/*
 * main.h
 *
 * Created on: Jan 20, 2017
 *     Author: Britney Reese
 *      Class: CS-222-02-17w
 */

//Header file for main.cpp

#ifndef MAIN_H_
#define MAIN_H_

#include <string>
using namespace std;

//Method prototypes

int base16to10(string s);
string fracToDec(int num, int den);
char toChar(int a);
string unroughen(string a, int b);

#endif /* MAIN_H_ */
