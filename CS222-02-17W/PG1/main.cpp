/*
 * main.cpp
 *
 * Created on: Jan 20, 2017
 *     Author: Britney Reese
 *      Class: CS-222-02-17w
 */

//This program takes a valid hexadecimal fraction and converts it to a hexadecimal decimal, using parenthesis to denote the repeating portion.

#include <iostream>
#include <string>
#include <cstdio>
#include <cmath>
#include "main.h"
using namespace std;

//Main method of the program; handles standard input and output, as wells a transforms the input using the appropriate functions

int main(int argc, char **argv) {

	string num16, den16;
	int num10, den10;
	cout << "Enter numerator: ";
	getline(cin, num16);
	cout << "Enter denominator: ";
	getline(cin, den16);
	num10 = base16to10(num16);
	den10 = base16to10(den16);
	cout << endl << num16 << "/"
				<< den16 << " = " << fracToDec(num10, den10)
				<< endl;
}

//Transforms a valid hexadecimal string into a decimal integer

int base16to10(string s) {

	int a = 0;
	for (int i = 0; i < (int) s.length(); i++) {
		int b = s[i] - '0';
		int c = s[i] - 'a';
		int d = s[i] - 'A';
		if (b >= 0 && b <= 9) {
			a += b * (int) pow(16, s.length() - 1 - i);
		} else if (c >= 0 && c <= 9) {
			a += (c + 10) * (int) pow(16, s.length() - 1 - i);
		} else if (d >= 0 && d <= 9) {
			a += (d + 10) * (int) pow(16, s.length() - 1 - i);
		}
	}
	return a;

}

//Transforms a decimal fraction represented as two integers into a hexadecimal decimal represented as a string

string fracToDec(int num, int den) {

	string a;
	int* hits = new int[den];
	for (int i = 0; i < den; i++) {
		hits[i] = -1;
	}
	int curRemainder = num;
	int prevRemainder = curRemainder;
	for (int counter = 0; hits[curRemainder] == -1; counter++) {
		hits[curRemainder] = counter;
		curRemainder = prevRemainder * 16 % den;
		int curQuotient = prevRemainder * 16 / den;
		a += toChar(curQuotient);
		prevRemainder = curRemainder;
	}
	a = unroughen(a, hits[curRemainder]);
	return a;

}

//Converts a decimal integer from 0-15 into a hexadecimal number represented by a char

char toChar(int a) {

	char b;
	if (a >= 0 && a <= 9) {
		b = a + '0';
	} else {
		b = a + 'A' - 10;
	}
	return b;

}

//Take the string of digits needed for the answer and the position of the start of the repeating portion and returns a formatted string as the answer

string unroughen(string rough, int firstParenPoint) {

	string nice = "0.";
	bool parenSet = false;
	for (int i = 0; i < (int) rough.length(); i++) {
		if (i == firstParenPoint && parenSet == false) {
			nice += "(";
			i--;
			parenSet = true;
		} else {
			nice += rough[i];
		}
	}
	nice += ")";
	return nice;

}
