/*
 * DiskItem.cpp
 *
 * Created on: Mar 1, 2017
 *     Author: Britney Reese
 *      Class: CS-222-02-17w
 */

#include "DiskItem.h"
using namespace std;

DiskItem::DiskItem(int s) {
	this->size = s;
}

int DiskItem::getsize() {
	return size;
}
