/*
 * Disk.cpp
 *
 * Created on: Mar 1, 2017
 *     Author: Britney Reese
 *      Class: CS-222-02-17w
 */

#include <iostream>
#include <string>
#include <cstdlib>
#include "Item.h"
#include "Disk.h"
using namespace std;

Disk::Disk(Item *i, Disk *b) {
	this->i = i;
	bellow = b;
}

Disk::~Disk() {
	delete i;
	delete bellow;
}

Item* Disk::geti() {return i;}

Disk * Disk::getbellow() {return bellow;}

void Disk::seti(Item* i) {this->i = i;}

void Disk::setbellow(Disk* b) {bellow = b;}
