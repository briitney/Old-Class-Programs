/*
 * HanoiMain.cpp
 *
 * Created on: Mar 1, 2017
 *     Author: Britney Reese
 *      Class: CS-222-02-17w
 */

#include "HanoiMain.h"
#include "DiskItem.h"
#include "DiskStack.h"
#include "Stack.h"
#include "HanoiMain.h"
using namespace std;

int main(int argc, char **argv) {
	cout << "Enter the number of disks you wish to use: ";
	int num;
	cin >> num;

	//Each of the pegs is a stack in an array
	Stack** stacks = new Stack* [3];
	stacks[0] = new DiskStack();
	stacks[1] = new DiskStack();
	stacks[2] = new DiskStack();

	//Populate the first peg with disks
	for (int i = num; i > 0; i--) {
		stacks[0]->PUSH(new DiskItem(i));
	}

	//Decide whether the disk of size 1 should go to the left or right
	int dir;
	if (num%2 == 0) dir = 1;
	else dir = 2;

	//Toggle every other execution of the loop
	int evn = 1;

	//Keep track of which peg has the disk of size one
	int d1 = 0;

	while (true){
		if (stacks[0]->ISEMPTY() && stacks[1]->ISEMPTY()) break;
		DiskItem * temp1, * temp2;
		//Even moves
		if (evn == 1) {
			//Move disk 1 to the stack in the pre-established direction.
			temp1 = (DiskItem *)(stacks[d1]->POP());
			stacks[(d1+dir)%3]->PUSH(temp1);
			int d2 = (d1+dir)%3;
			cout << "Move disk 1 from pole " << d1+1 << " to pole " << d2+1 << "." << endl;
			d1 = d2;
		} else {
		//Odd moves
			int p1, p2, v1, v2, a, b, c;
			//Get info on the pegs which do not have disk 1
			p1 = (d1+1)%3;
			p2 = (d1+2)%3;
			temp1 = (DiskItem *)(stacks[p1])->POP();
			if (temp1)
				v1 = temp1->getsize();
			else v1 = 0;
			temp2 = (DiskItem *)(stacks[p2])->POP();
			if (temp2)
				v2 = temp2->getsize();
			else v2 = 0;
			//Declare what happens in each of the four possible situations
			if (v2 == 0) {
				a = v1;
				b = p1;
				c = p2;
				stacks[p2]->PUSH(temp1);
			} else if (v1 == 0) {
				a = v2;
				b = p2;
				c = p1;
				stacks[p1]->PUSH(temp2);
			} else if (v1 < v2) {
				a = v1;
				b = p1;
				c = p2;
				stacks[p2]->PUSH(temp2);
				stacks[p2]->PUSH(temp1);
			} else if (v1 > v2) {
				a = v2;
				b = p2;
				c = p1;
				stacks[p1]->PUSH(temp1);
				stacks[p1]->PUSH(temp2);
			}
			cout << "Move disk " << a << " from pole " << b+1 << " to pole " << c+1 << "." << endl;
		}
		evn = (++evn)%2;
	}
	for (int i = 0; i < 3; i++) {
		delete stacks[i];
	}
	delete[] stacks;
	return 0;
}

//Used for debugging, would print out the value of the disk currently on top of the stack.
void readEm (Stack** stcks) {
	for (int i = 0; i < 3; i++){
		DiskItem *temp1 = (DiskItem *)stcks[i]->POP();
		if (temp1){
			cout << "Pole " << i+1 << ": " << temp1->getsize() << "\t";
			stcks[i]->PUSH(temp1);
		} else {
			cout << "Pole " << i+1 << ": 0\t";
		}
	} cout << endl;
}
