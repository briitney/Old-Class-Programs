/*
 * Disk.h
 *
 * Created on: Mar 1, 2017
 *     Author: Britney Reese
 *      Class: CS-222-02-17w
 */

//LinkedList Node with different vocabulary

#ifndef DISK_H_
#define DISK_H_

#include <iostream>
#include <string>
#include <cstdlib>
#include "Item.h"
using namespace std;

class Item;
class Disk {
private:
	Item *i;
	Disk *bellow;
public:
	Disk(Item *i, Disk *b);
	~Disk();
	Item * geti();
	Disk * getbellow();
	void seti(Item *i);
	void setbellow(Disk *n);
};

#endif /* DISK_H_ */
