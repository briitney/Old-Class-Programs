/*
 * DiskItem.h
 *
 * Created on: Mar 1, 2017
 *     Author: Britney Reese
 *      Class: CS-222-02-17w
 */

//Item which holds an int

#ifndef DISKITEM_H_
#define DISKITEM_H_

#include <iostream>
#include <string>
#include <cstdlib>
#include "Item.h"
using namespace std;

class Item;
class DiskItem : public Item {
private:
	int size;
public:
	int getsize();
	DiskItem(int s);
};

#endif /* DISKITEM_H_ */
