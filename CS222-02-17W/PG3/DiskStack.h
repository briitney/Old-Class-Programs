/*
 * DiskStack.h
 *
 * Created on: Mar 1, 2017
 *     Author: Britney Reese
 *      Class: CS-222-02-17w
 */

//LinkedList Stack with different vocabulary

#ifndef DISKSTACK_H_
#define DISKSTACK_H_

#include <iostream>
#include <string>
#include <cstdlib>
#include "Stack.h"
#include "Item.h"
#include "Disk.h"
using namespace std;

class Stack;
class Item;

class DiskStack : public Stack {
private:
	Disk *top;
public:
	virtual void PUSH(Item* i);
	virtual Item* POP();
	virtual bool ISEMPTY();
	DiskStack();
	~DiskStack();
};

#endif /* DISKSTACK_H_ */
