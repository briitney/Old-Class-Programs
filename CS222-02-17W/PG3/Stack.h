//Code Provided by Andy Poe for Data Structures of Winter 2017

#ifndef _STACK_
#define _STACK_

#include <iostream>
#include <string>
#include <cstdlib>
#include "Item.h"
using namespace std;

class Item;
class Stack {

public:
	virtual void PUSH(Item *i) = 0;
	virtual Item * POP() = 0;
	virtual bool ISEMPTY() = 0;
};

//Virtual means that I can override the method when I inherit the class.
//=0 means abstract which means no code at all; I HAVE TO override the method

#endif
