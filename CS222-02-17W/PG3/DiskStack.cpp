/*
 * DiskStack.cpp
 *
 * Created on: Mar 1, 2017
 *     Author: Britney Reese
 *      Class: CS-222-02-17w
 */

#include <iostream>
#include <string>
#include <cstdlib>
#include "Item.h"
#include "Disk.h"
#include "DiskStack.h"
using namespace std;

DiskStack::DiskStack() {
	top = NULL;
}

DiskStack::~DiskStack () {
	delete top;
}

void DiskStack::PUSH (Item* i) {
	top = new Disk(i, top);
}

Item* DiskStack::POP() {
	if (ISEMPTY()) return NULL;
	Disk* tt = top->getbellow();
	Item *i = top->geti();
	top->setbellow(NULL);
	top->seti(NULL);
	delete top;
	top = tt;
	return i;
}

bool DiskStack::ISEMPTY() {
	return !top;
}
