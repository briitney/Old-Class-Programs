/*
 * HanoiMain.h
 *
 * Created on: Mar 1, 2017
 *     Author: Britney Reese
 *      Class: CS-222-02-17w
 */

#ifndef HANOIMAIN_H_
#define HANOIMAIN_H_

#include <iostream>
#include <string>
#include <cstdlib>
#include "DiskItem.h"
#include "DiskStack.h"
using namespace std;

void readEm (DiskStack** stcks);

#endif /* HANOIMAIN_H_ */
