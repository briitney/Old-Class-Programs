/*
 * RBDBTree.h
 *
 * Created on: Apr 14, 2017
 *     Author: Britney Reese
 *      Class: CS-222-02-17w
 */

#ifndef RBDBTREE_H_
#define RBDBTREE_H_

#include "Database.h"
#include "DBTree.h"
#include "DRT.h"
#include "Leaf.h"
#include "RBLeaf.h"
#include "KeyValue.h"
using namespace std;

class DBTree;
class RBDBTree;
class RBLeaf;
class RBDBTree : public DBTree {
protected:
	bool red, black;
public:
	RBDBTree() : DBTree() { root = NULL; red = true; black = false;}
	virtual void remove(Leaf * dleaf);
	virtual void add(string key, string data);
	void setRoot(RBLeaf * newroot);
	RBLeaf * getRoot();

	int Valid ();
};

#endif /* RBDBTREE_H_ */
