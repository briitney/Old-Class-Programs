/*
 * RBLeaf.cpp
 *
 * Created on: Apr 14, 2017
 *     Author: Britney Reese
 *      Class: CS-222-02-17w
 */

//Inherits from Leaf, but kinda is a pain because I'm typecasting from (Leaf *) to (RBLeaf *) all the time.

#include <string>
#include "RBLeaf.h"
#include "Leaf.h"
#include "Item.h"
#include "RBDBTree.h"
//debug
#include "KeyValue.h"
using namespace std;

//Adjust the DBTree's add.
void RBLeaf::addAdjust() {
	RBLeaf * x = this;
	RBLeaf * p;
	RBLeaf * u = NULL;
	RBLeaf * gp = NULL;
	while (true) {
		if (x->getParent() == NULL) {
			x->setColor(black);
			break;
		}
		p = (RBLeaf *) x->getParent();
		if (p->getColor() == black) {
			break;
		}
		gp = (RBLeaf *)p->getParent();
		u = (RBLeaf * )p->getSibling();
		if (u && u->getColor() == red) {
			u->setColor(black);
			p->setColor(black);
			gp->setColor(red);
			x = gp;
			continue;
		}
		if ( (p == gp->getLeft() && x == p ->getLeft()) ||
			(p == gp->getRight() && x == p ->getRight())
			) {
			p->rotate();
			break;
		}
		x->rotate();
		x->rotate();
		break;
	}
}

//Had to re-do the remove, because I need to assign p and x before the method is over.
//I didn't need a return value, so set to make it return NULL.
Leaf * RBLeaf::remove(Leaf * dleaf){
	if (item->compare(dleaf->getItem())== 0) {
		if (!left && !right) {
			removeAdjust(this);
		} else if (!left && right) {
			((RBLeaf *)right->getLeftDown())->removeAdjust(this);
		} else if (left && !right) {
			((RBLeaf *)left->getRightDown())->removeAdjust(this);
		} else if (left && right) {
			((RBLeaf *)right->getLeftDown())->removeAdjust(this);
		}
	} else {
		if (item->compare(dleaf->getItem()) > 0 && left) left->remove(dleaf);
		else if (right) right->remove(dleaf);
	}
	return NULL;
}

//Does all the step detailed in the RB Rules sheet
void RBLeaf::removeAdjust(RBLeaf * deleted) {
	RBLeaf * p = NULL;
	RBLeaf * x = NULL;
	RBLeaf * w = NULL;
	if (parent) p = (RBLeaf *)parent;
	if (right) x = (RBLeaf *)right;
	if (left) x = (RBLeaf *)left;
	bool dcolor = color;
	//Delete D
	if (p) {
		if (p->getRight() == this) p->setRight(x);
		else if (p->getLeft() == this) p->setLeft(x);
		if (x) x->setParent(p);
	} else {
		if (x) x->setParent(NULL);
		base->setRoot(x);
	}
	delete deleted->getItem();
	deleted->setItem(item);
	item = NULL;
	delete this;
	//If D was red
	if (dcolor == red) return;
	while (true) {
		//If X is red, color X black, DONE!
		if (x && x->getColor() == red) {
			//cout << "Rule 4" << endl;
			x->setColor(black);
			break;
		}
		//If X is the root, DONE!
		if (p == NULL) break;
		//Create W
		if (x == p->getLeft()) {
			w = (RBLeaf *)p->getRight();
		} else {
			w = (RBLeaf *)p->getLeft();
		}
		//If W is red, rotate W once, RESTART
		if (w->getColor() == red) {
			//cout << "Rule 7" << endl;
			w->rotate();
			continue;
		}
		//If W's children are both black, RESTART
		if ((!w->getDirectChild() || ((RBLeaf *)w->getDirectChild())->getColor() == black) &&
			(!w->getZigZagChild() || ((RBLeaf *)w->getZigZagChild())->getColor() == black)
		) {
			//cout << "Rule 8" << endl;
			w->setColor(red);
			x = p;
			p = (RBLeaf *)p->getParent();
			continue;
		}
		//If W's direct child is red, color black and rotate, DONE!
		if (w->getDirectChild() && (((RBLeaf *)w->getDirectChild())->getColor() == red)) {
			//cout << "Rule 9" << endl;
			((RBLeaf *)w->getDirectChild())->setColor(black);
			w->rotate();
			break;
		}
		//cout << "Rule 10" << endl;
		//Rotate the zig-zag twice, recolor W black, DONE!
		RBLeaf * zzc = ((RBLeaf *)w->getZigZagChild());
		zzc->rotate();
		zzc->rotate();
		w->setColor(black);
		break;
	}
}

//Helper methods

bool RBLeaf::getColor() {
	return color;
}

void RBLeaf::setColor(bool color) {
	this->color = color;
}

bool color(RBLeaf * leaf) {
	if (!leaf) return false;
	return leaf->getColor();
}

Leaf * RBLeaf::getSibling() {
	if (!parent) return NULL;
	if (this == parent->getRight()) return parent->getLeft();
	return parent->getRight();
}

Leaf * RBLeaf::getDirectChild(){
	if (!parent) return NULL;
	if (this == parent->getRight()) return right;
	return left;
}

Leaf * RBLeaf::getZigZagChild(){
	if (!parent) return NULL;
	if (this == parent->getRight()) return left;
	return right;
}

//Rotate a node up, changing appropriate pointers
void RBLeaf::rotate() {
	if (!parent) cout << "This should never get called." << endl;
	RBLeaf * x = this;
	bool xColor = color;
	RBLeaf * p = (RBLeaf *)parent;
	bool pColor = p->getColor();
	RBLeaf * gp = (RBLeaf *)p->getParent();
	RBLeaf * z = (RBLeaf *)getZigZagChild();

	x->setColor(pColor);
	p->setColor(xColor);

	x->setParent(gp);
	if(gp == NULL) {
		base->setRoot(x);
	} else if (gp->getRight() == p) {
		gp->setRight(x);
	} else {
		gp->setLeft(x);
	}

	p->setParent(x);

	if (p->getRight() == x) {
		x->setLeft(p);
		p->setRight(z);
	} else {
		x->setRight(p);
		p->setLeft(z);
	}

	if (z != NULL) {
		z->setParent(p);
	}
}

//Helpful for debugging
void RBLeaf::PrintInOrder(int i) {
	if (left) ((RBLeaf *)left)->PrintInOrder(i+1);
	if (parent == NULL) cout << "root - ";
	cout << "Name: " << ((KeyValue *)item)->getKey() << ",\tLevel:" << i << ",\t" << "Color: " << color << "." << endl;
	if (right) ((RBLeaf *)right)->PrintInOrder(i+1);
}

//Andy's validity method
int RBLeaf::ValidNode() {
	int lc, rc, r;

	if (!(color == black) && parent && !(((RBLeaf* )parent)->getColor() == black)) {
		r= -1;
		cout << "ERROR: You and your parent are both red!" << endl;
	} else {
		if (left && left->getParent() != this) {
			r= -1;
			cout << "ERROR: Your left does not point back to you!" << endl;
		} else {
			if (left && left->getItem()->compare(item) >= 0) {
				r= -1;
				cout << "ERROR: You are bigger than your left!" << endl;
			} else {
				if (right && right->getParent() != this) {
					r= -1;
					cout << "ERROR: Your right does not point back to you!" << endl;
				} else {
					if (right && right->getItem()->compare(item) <= 0) {
						r= -1;
						cout << "ERROR: Your right is less than you!" << endl;
					} else {
						if (left) {
							lc = ((RBLeaf *)left)->ValidNode ();
						} else {
							lc = 0;
						}
						if (lc==-1) {
							cout << "ERROR: Left side is invalid" << endl;
							r= -1;
						} else {
							if (right) {
								rc = ((RBLeaf *)right)->ValidNode();
							} else {
								rc = 0;
							}
							if (rc==-1) {
								cout << "ERROR: Right side is invalid" << endl;
								r= -1;
							} else {
								if (lc != rc) {
									cout << "ERROR: Black count is off balance at node \"" << ((KeyValue *)item)->getKey() << "\""  << endl;
									r = -1;
								} else {
									if (getColor() == black) {
										r = lc+1;
									} else {
										r = lc;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	return r;
}
