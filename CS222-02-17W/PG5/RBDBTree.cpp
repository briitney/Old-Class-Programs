/*
 * RBDBTree.cpp
 *
 * Created on: Apr 14, 2017
 *     Author: Britney Reese
 *      Class: CS-222-02-17w
 */

//Inherits from DBTree, and uses all of it's method besides add and delete (and some helpers)

#include "Database.h"
#include "DBTree.h"
#include "RBDBTree.h"
#include "DRT.h"
#include "Leaf.h"
#include "RBLeaf.h"
#include "KeyValue.h"
using namespace std;

void RBDBTree::remove(Leaf * dleaf){
	if (root) ((RBLeaf *)root)->remove(dleaf);
	//if (root) ((RBLeaf *)root)->PrintInOrder(1);
	//if (Valid() != 1) exit(2);

}

//Was able to use the DBTree add and just have a method do the balancing.
void RBDBTree::add(string key, string data) {
	RBLeaf * newleaf = new RBLeaf(NULL, new KeyValue(key, data), this);
	if (root) ((Leaf *)root)->add(newleaf);
	else root = newleaf;
	newleaf->addAdjust();
	//((RBLeaf *)root)->PrintInOrder(1);
	//if (Valid() != 1) exit(2);
}

void RBDBTree::setRoot(RBLeaf * newroot) {
	root = newroot;
}

RBLeaf * RBDBTree::getRoot() {
	return (RBLeaf *)root;
}

//Andy's Validity method
int RBDBTree::Valid() {
	int r;

	if (!root) {
		r=1;
	} else {
		if (((RBLeaf *)root)->getColor() == red) {
			r=0;
		} else {
			r=((RBLeaf *)root)->ValidNode() != -1;
		}
	}
	return r;
}

