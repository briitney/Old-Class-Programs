/*
 * main.cpp
 *
 * Created on: Mar 28, 2017
 *     Author: Britney Reese
 *      Class: CS-222-02-17w
 */

#include "main.h"
#include "Database.h"
#include "DBTree.h"
#include "RBDBTree.h"
#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
using namespace std;

//Handle input, check input validity, and delegate tasks
int main(int argc, char **argv) {
	cout << "Database active. Enter commands." << endl;
	string command = "", arg1 = "", arg2 = "";
	//Change main from using a DBTree to using a RBDBTree.
	Database * DB = new RBDBTree();
	//fstream cin("file.in");
	while (true) {
		command = "", arg1 = "", arg2 = "";
		getline(cin, command);
		getCommand(command, arg1, arg2);
		cout << command;
		if (arg1 != "") cout << " \"" << arg1 << "\"";
		if (arg2 != "") cout << " \"" << arg2 << "\"";
		cout << endl;
		if (command == "ADD") {
			if (arg1 != "" && arg2 != "") DB->modify(arg1, arg2);
			else cout << "Not enough arguments." << endl;
		} else if (command == "PRINT") {
			print(DB);
		} else if (command == "RPRINT") {
			rprint(DB);
		} else if (command == "REMOVE") {
			if (arg1 == "") cout << "Argument needed." << endl;
			else if (false && arg2 != "") cout << "Only one argument." << endl;
			else {
				DRT * answer = DB->modify(arg1, "");
				if (answer->getdata() == "") cout << "Student '" << arg1 << "' not found." << endl;
				delete answer;
			}
		} else if (command == "LOOKUP") {
			if (arg2 != "") cout << "Only one argument allowed." << endl;
			else {
				DRT * answer = DB->search(arg1);
				if (answer->getdata() != "") {
					cout << "Student: " << arg1 << "\tGrade: " << answer->getdata() << endl;
				} else {
					cout << "Student '" << arg1 << "' not found.";
					if (answer->getprev() != "") cout << " The student before is '" << answer->getprev() << "'";
					else cout << " There is no student before";
					cout << " and";
					if (answer->getnext() != "") cout << " the student after is '" << answer->getnext() << "'";
					else cout << " there is no student after";
					cout << "." << endl;
				}
				delete answer;
			}
		} else if (command == "EXIT") {
			cout << "Goodbye." << endl;
			break;
		} else {
			cout << "I do not understand that command." << endl;
		}
	}
	delete DB;
	return 0;
	//cin.close();
}

//parse the string for the commands and arguments
void getCommand(string &command, string &arg1, string &arg2) {
	string s = command;
	command = "";
	int var = 0;
	int quotecount = 0;
	for (int i = 0; i < (int)(s.length()); i++) {
		if (var == 0) {
			if (s[i] == ' ' || s[i] == '\r' || s[i] == '\n' || (int)s[i] == 13) {
				var = 1;
			} else {
				command += s[i];
			}
		} else if (var == 1) {
			if (s[i] == '\"' || s[i] == ' ' || s[i] == '\r' || s[i] == '\n' || (int)s[i] == 13) {
				if (s[i] == '\"') quotecount++;
				if (quotecount == 1 && s[i] == ' ') arg1 += s[i];
			} else {
				arg1 += s[i];
			}
			if (quotecount == 2) {
				var++;
				quotecount = 0;
			}
		} else {
			if (s[i] == '\"' || s[i] == ' ' || s[i] == '\r' || s[i] == '\n' || (int)s[i] == 13) {
				if (s[i] == '\"') quotecount++;
				if (quotecount == 1 && s[i] == ' ') arg2 += s[i];
			} else {
				arg2 += s[i];
			}
		}
	}
}

//print by searching for a record and finding it's next
void print (Database * DB) {
	DRT * res = DB->search("");
	do {
		if (res->getnext() != "") cout << "Student: '" << res->getnext() << "'";
		DRT * newres = DB->search(res->getnext());
		delete res;
		res = newres;
		if (res->getdata() != "") if (res->getdata() != "") cout << "\tGrade: '" << res->getdata() << "'" << endl;
	} while (res->getnext() != "");
	delete res;
}

//print by searching for a record and finding it's previous
void rprint (Database * DB) {
	DRT * res = DB->search("");
	do {
		if (res->getprev() != "") cout << "Student: '" << res->getprev() << "'";
		DRT * newres = DB->search(res->getprev());
		delete res;
		res = newres;
		if (res->getdata() != "") cout << "\tGrade: '" << res->getdata() << "'" << endl;
	} while (res->getprev() != "");
	delete res;
}
