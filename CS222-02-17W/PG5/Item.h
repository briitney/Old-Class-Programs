//Code Provided by Andy Poe for Data Structures of Winter 2017

#ifndef _ITEM_
#define _ITEM_

#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;

class Item {
public:
	virtual int compare(Item * other) = 0;
	virtual ~Item(){};
};

#endif
