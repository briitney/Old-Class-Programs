/*
 * KeyValue.cpp
 *
 * Created on: Mar 27, 2017
 *     Author: Britney Reese
 *      Class: CS-222-02-17w
 */

#include "KeyValue.h"

#include <iostream>
#include <string>
#include <cstdlib>
#include "Item.h"
using namespace std;


KeyValue::KeyValue(string key, string value) {
	this->key = key;
	this->value = value;
}

KeyValue::KeyValue(string key) {
	this->key = key;
	this->value = "";
}

//only works when comparing another keyvalue
int KeyValue::compare(Item * other) {
	try {
		if (key == ((KeyValue *)other)->getKey()) return 0;
		else if (key < ((KeyValue *)other)->getKey()) return -1;
		return 1;
	} catch (...) {
		cout << "Could not compare Items." << endl;
		return 0;
	}
}

string KeyValue::getValue() {
	return this->value;
}

void KeyValue::setValue(string s) {
	this->value = s;
}

string KeyValue::getKey() {
	return this->key;
}
