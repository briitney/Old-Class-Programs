/*
 * DBTree.h
 *
 * Created on: Mar 22, 2017
 *     Author: Britney Reese
 *      Class: CS-222-02-17w
 */

#ifndef DBTREE_H_
#define DBTREE_H_

#include "Database.h"
#include "DRT.h"
#include "Leaf.h"
#include "KeyValue.h"
using namespace std;

class DBTree;
class DBTree : public Database {
protected:
	Leaf * root;
public:
	DBTree();
	DRT * search (string key);
	DRT * modify (string key, string data);
	virtual void remove(Leaf * dleaf);
	virtual void add(string key, string data);
	DRT * buildDRT(Leaf * prev, Leaf * result, Leaf * next);
	~DBTree();
	void printTree();
};

#endif /* DBTREE_H_ */
