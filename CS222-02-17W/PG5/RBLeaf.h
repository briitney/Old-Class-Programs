/*
 * RBLeaf.h
 *
 * Created on: Apr 14, 2017
 *     Author: Britney Reese
 *      Class: CS-222-02-17w
 */

#ifndef RBLEAF_H_
#define RBLEAF_H_

#include "Item.h"
#include "Leaf.h"
#include "RBDBTree.h"
//debug
#include "Item.h"
using namespace std;

class Leaf;
class RBLeaf;
class RBDBTree;
class RBLeaf : public Leaf {
private:
	bool color;
	const static bool red = true;
	const static bool black = false;
	RBDBTree * base;

public:
	RBLeaf (Leaf * parent, Item * item, RBDBTree * base): Leaf(parent, item) {
		this->parent = parent;
		this->left = NULL;
		this->right = NULL;
		this->item = item;
		this->color = red;
		this->base = base;
	}

	RBLeaf(Leaf * parent, Item * item, Leaf * left, Leaf * right, RBDBTree * base) : Leaf(parent, item, left, right){
		this->parent = parent;
		this->left = left;
		this->right = right;
		this->item = item;
		this->color = red;
		this->base = base;
	}
	void addAdjust();
	virtual Leaf * remove(Leaf * dleaf);
	void removeAdjust(RBLeaf * deleted);
	bool getColor();
	void setColor(bool color);
	Leaf * getSibling();
	Leaf * getDirectChild();
	Leaf * getZigZagChild();
	Leaf * getZigZagGrandchild();
	RBDBTree * getBase();
	void rotate();
	void PrintInOrder(int i);
	int ValidNode();
};

#endif /* RBLEAF_H_ */
