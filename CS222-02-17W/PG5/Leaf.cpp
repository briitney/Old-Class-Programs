/*
 * Leaf.cpp
 *
 * Created on: Mar 27, 2017
 *     Author: Britney Reese
 *      Class: CS-222-02-17w
 */

#include <string>
#include "Leaf.h"
#include "Item.h"
using namespace std;

Leaf::Leaf(Leaf * parent, Item * item) {
	this->parent = parent;
	this->left = NULL;
	this->right = NULL;
	this->item = item;
}

Leaf::Leaf(Leaf * parent, Item * item, Leaf * left, Leaf * right) {
	this->parent = parent;
	this->left = left;
	this->right = right;
	this->item = item;
}

Item * Leaf::getItem() {
	return item;
}

void Leaf::setItem(Item * item) {
	this->item = item;
}

Leaf * Leaf::getParent() {
	return parent;
}

void Leaf::setParent(Leaf* parent) {
	this->parent = parent;
}

Leaf * Leaf::getLeft() {
	return left;
}

void Leaf::setLeft(Leaf * left) {
	this->left = left;
}

Leaf * Leaf::getRight() {
	return right;
}

void Leaf::setRight(Leaf * right) {
	this->right = right;
}

//add a leaf to the tree
void Leaf::add(Leaf * nleaf) {
	if (item->compare(nleaf->getItem()) > 0) {
		if (left) {
			left->add(nleaf);
		} else {
			nleaf->setParent(this);
			left = nleaf;
		}
	} else {
		if (right) {
			right->add(nleaf);
		} else {
			nleaf->setParent(this);
			right = nleaf;
		}
	}
}

//remove a leaf from the tree
Leaf* Leaf::remove(Leaf * dleaf) {
	if (item->compare(dleaf->getItem()) == 0) {
		if (!left && !right) {
			delete this;
			return NULL;
		}
		if (left && !right) {
			left->setParent(parent);
			Leaf * temp = left;
			delete this;
			return temp;
		}
		if (right && !left) {
			right->setParent(parent);
			Leaf * temp = right;
			delete this;
			return temp;
		}
		if (right && left) {
			Leaf * temp = right->getLeftDown();
			if (right != temp) {
				temp->getParent()->setLeft(temp->getRight());
				if (temp->getRight()) temp->getRight()->setParent(temp->getParent());
				temp->setRight(right);
				right->setParent(temp);
			}
			temp->setParent(parent);
			temp->setLeft(left);
			left->setParent(temp);
			delete this;
			return temp;
		}
		return NULL;
	} else {
		if (item->compare(dleaf->getItem()) > 0 && left) left = left->remove(dleaf);
		else if (right) right = right->remove(dleaf);
	}
	return this;
}

void Leaf::destroy() {
	if (left) left->destroy();
	if (right) right->destroy();
	delete this;
}

//search for justs the leaf
Leaf * Leaf::search(Item * s) {
	if (item->compare(s) == 0) {
		return this;
	}
	if (item->compare(s) > 0 && left) {
		return left->search(s);
	}
	if (item->compare(s) < 0 && right) {
		return right->search(s);
	}
	return NULL;
}

//search for a leaf, it's previous, and it's next
Leaf ** Leaf::searchSet(Item * s, Leaf ** leaves) {
	if (item->compare(s) == 0) {
		leaves[1] = this;
		if (left) {
			leaves[0] = left->getRightDown();
		}
		if (right) {
			leaves[2] = right->getLeftDown();
		}
		return leaves;
	}
	if (item->compare(s) > 0) {
		leaves[2] = this;
		if (left) return left->searchSet(s, leaves);
	}
	if (item->compare(s) < 0) {
		leaves[0] = this;
		if (right) return right->searchSet(s, leaves);
	}
	return leaves;
}

//get the leaf all the way down on the left
Leaf * Leaf::getLeftDown() {
	if (left) {
		return left->getLeftDown();
	}
	return this;
}

//get the leaf all the way down on the right
Leaf * Leaf::getRightDown() {
	if (right) {
		return right->getRightDown();
	}
	return this;
}

Leaf::~Leaf() {
	delete item;
}
