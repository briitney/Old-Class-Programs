/*
 * KeyValue.h
 *
 * Created on: Mar 27, 2017
 *     Author: Britney Reese
 *      Class: CS-222-02-17w
 */

#ifndef KEYVALUE_H_
#define KEYVALUE_H_

#include <iostream>
#include <string>
#include <cstdlib>
#include "Item.h"
using namespace std;

class Item;
class KeyValue : public Item {
private:
	string key;
	string value;
public:
	KeyValue(string key, string value);
	KeyValue(string key);
	virtual int compare(Item * other);
	string getValue();
	void setValue(string s);
	string getKey();
};

#endif /* KEYVALUE_H_ */
