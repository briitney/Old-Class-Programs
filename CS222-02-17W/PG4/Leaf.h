/*
 * Leaf.h
 *
 * Created on: Mar 27, 2017
 *     Author: Britney Reese
 *      Class: CS-222-02-17w
 */

#ifndef LEAF_H_
#define LEAF_H_
#include "Item.h"
using namespace std;

class Leaf;
class Leaf {
private:
	Leaf * left;
	Leaf * right;
	Leaf * parent;
	Item * item;

public:
	Leaf(Leaf * parent, Item * item);
	Leaf(Leaf * parent, Item * item, Leaf * left, Leaf * right);
	Item * getItem();
	void setItem(Item * item);
	Leaf * getParent();
	void setParent(Leaf* parent);
	Leaf * getLeft();
	void setLeft(Leaf * left);
	Leaf * getRight();
	void setRight(Leaf * Right);
	void add(Leaf * nleaf);
	Leaf * remove(Leaf * dleaf);
	void destroy();
	Leaf * search(Item * s);
	Leaf ** searchSet(Item * s, Leaf ** leaves);
	Leaf * getLeftDown();
	Leaf * getRightDown();
	~Leaf();
	string printTree();
};

#endif /* LEAF_H_ */
