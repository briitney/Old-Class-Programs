/*
 * DBTree.cpp
 *
 * Created on: Mar 27, 2017
 *     Author: Britney Reese
 *      Class: CS-222-02-17w
 */

#include "Database.h"
#include "DRT.h"
#include "Leaf.h"
#include "KeyValue.h"
#include "DBTree.h"
using namespace std;

DBTree::DBTree(){
	root = NULL;
}

//Find a leaf, it's previous, and next, and pack it into a DRT
DRT * DBTree::search (string key) {
	KeyValue * searchTerm = new KeyValue(key);
	Leaf ** results = new Leaf * [3];
	for (int i = 0; i < 3; i++) results[i] = NULL;
	if (key != "" && root) results = root->searchSet(searchTerm, results);
	else if (root) {
		results[2] = root->getLeftDown();
		results[0] = root->getRightDown();
	}
	delete searchTerm;
	Leaf * result = results[1];
	Leaf * prev = results[0];
	Leaf * next = results[2];
	delete[] results;
	return buildDRT(prev, result, next);
}

//Find a leaf, it's previous, and next.
//If key not found, create a new copy.
//Else, if key found, either modify it's data or delete it.
DRT * DBTree::modify (string key, string data) {
	KeyValue * searchTerm = new KeyValue(key);
	Leaf ** results = new Leaf * [3];
	for (int i = 0; i < 3; i++) results[i] = NULL;
	if (key != "" && root) results = root->searchSet(searchTerm, results);
	else if (root) {
		results[2] = root->getLeftDown();
		results[0] = root->getRightDown();
	}
	delete searchTerm;
	Leaf * result = results[1];
	Leaf * prev = results[0];
	Leaf * next = results[2];
	DRT * dirt = buildDRT(prev, result, next);
	delete[] results;
	if (result) {
		if (data != "") {
			((KeyValue *)result->getItem())->setValue(data);
		} else {
			remove(result);
			result = NULL;
		}
	}
	else if (key != "" && data != "") add(key, data);
	return dirt;
}

//remove a leaf recursively
void DBTree::remove(Leaf * dleaf) {
	if (root) root = root->remove(dleaf);
}

//add a leaf recursively
void DBTree::add(string key, string data) {
	if (root) root->add(new Leaf(NULL, new KeyValue(key, data)));
	else root = new Leaf(NULL, new KeyValue(key, data));
}

//create a DRT from the leaves in the result
DRT * DBTree::buildDRT(Leaf * prev, Leaf * result, Leaf * next) {
	string pr = "", res = "", nxt = "";
	if (prev) {
		pr = ((KeyValue *)prev->getItem())->getKey();
	}
	if (result) {
		res = ((KeyValue *)result->getItem())->getValue();
	}
	if (next) {
		nxt = ((KeyValue *)next->getItem())->getKey();
	}
	return new DRT(res, nxt, pr);
}

DBTree::~DBTree() {
	if (root) root->destroy();
	else delete root;
}

