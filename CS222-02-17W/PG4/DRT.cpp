//Code Provided by Andy Poe for Data Structures of Winter 2017

#include <string>
#include "DRT.h"
#include <iostream>
#include <cstdlib>
using namespace std;

DRT::DRT (string d, string n, string p) {
 data = d; next = n; prev = p;
}

string DRT::getdata () {return data;}

string DRT::getnext () {return next;}

string DRT::getprev () {return prev;}
