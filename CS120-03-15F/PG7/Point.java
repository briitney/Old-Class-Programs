//Britney Reese CS 120-01-15f
//This is a class for a point
import java.io.*;
import java.util.*;

public class Point {

	private double x, y;

	public Point (double a, double b) {
		x = a;
		y = b;
	}

	public double getX () {
		return x;
	}

	public double getY () {
		return y;
	}

	//Calculates distance
	public double Distance (Point B) throws Exception {
		return Math.pow(Math.pow(x-B.getX(),2)+Math.pow(y-B.getY(),2),0.5);
	}

}