//Britney Reese CS 120-01-15f
//This makes a triangle and finds the perimeter and area
import java.io.*;
import java.util.*;

public class TriangleMain {

	private static Scanner input;

	public static void main (String[] args) throws Exception {

		input = new Scanner(System.in);
		System.out.println("Enter three coordinate points: ");
		double a = Double.parseDouble(input.nextLine());
		double b = Double.parseDouble(input.nextLine());
		double c = Double.parseDouble(input.nextLine());
		double d = Double.parseDouble(input.nextLine());
		double e = Double.parseDouble(input.nextLine());
		double f = Double.parseDouble(input.nextLine());
		//Make triangle
		Triangle A = new Triangle(a,b,c,d,e,f);
		//Calculates results
		System.out.println(A.Perimeter());
		System.out.println(A.Area());


	}

}