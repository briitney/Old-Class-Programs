//Britney Reese CS 120-01-15f
//This makes a triangle
import java.io.*;
import java.util.*;

public class TriangleOld {

	private PointOld a, b, c;

	//This contructs a triagle out of points
	public TriangleOld
	(double x1, double y1,
	double x2, double y2,
	double x3, double y3) throws Exception {
		a = new PointOld (x1, y1);
		b = new PointOld (x2, y2);
		c = new PointOld (x3, y3);
	}

	//This finds the perimeter
	public double Perimeter () throws Exception {
		return a.Distance(b)+b.Distance(c)+c.Distance(a);
	}

	//This finds the area
	public double Area () throws Exception {
		double s = Perimeter()/2.0;
		return Math.pow(s*(s-a.Distance(b))*(s-b.Distance(c))*(s-c.Distance(a)),0.5);
	}

}