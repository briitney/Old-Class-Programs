//Britney Reese CS 120-03-15f
//This program has a user click three points, then draws a triangle with those points and computes it's area and perimeter.
import java.applet.*;
import java.awt.*;
import java.awt.event.*;

public class DrawTriangle extends Applet {

	Point[] x;
	Color c = new Color(0,0,0);

	public void init () {
		x = new Point[0];
		resize (500,500);
		//Create new mose listener
		addMouseListener (new MouseAdapter() {
			public void mousePressed (MouseEvent e) {
				//Adds new point to array of points
				Point[] a = new Point[x.length+1];
				for (int i = 0; i < x.length; i++)
					a[i] = x[i];
				a[x.length] = new Point(e.getX(), e.getY());
				//Picks color
				if (between(e.getX(), 2, 52) && between(e.getY(), 62, 262)) {
					if (between(e.getY(), 62, 112)) c = Color.red;
					else if (between(e.getY(), 112, 162)) c = Color.green;
					else if (between(e.getY(), 162, 212)) c = Color.blue;
					else if (between(e.getY(), 212, 262)) c = Color.black;
				}
				//Makes it so you can't click inside the result's box
				else if (between(e.getX(), 2, 252) && between(e.getY(),2,52)) {
				}
				else {
					x=a;
				}
				repaint();
			}
		});
	}

	//Finds if the num value is between the last two
	public Boolean between (int num, int first, int last) {
		if (first<num && num<last) return true;
		return false;
	}

	public void paint (Graphics g) {

		//draws where area and perimeter will be shown, and color picker
		g.drawRect(2, 2, 250, 50);
		g.drawString("Area: ", 6, 20);
		g.drawString("Perimeter: ", 6, 40);

		g.setColor(Color.red);
		g.fillRect(2, 62, 50, 50);

		g.setColor(Color.green);
		g.fillRect(2, 112, 50, 50);

		g.setColor(Color.blue);
		g.fillRect(2, 162, 50, 50);

		g.setColor(Color.black);
		g.fillRect(2, 212, 50, 50);

		g.setColor(c);

		for (int i = 0; i < x.length; i++) {
			//draw point
			g.fillOval ((int)x[i].getX()-4,(int)x[i].getY()-4, 8, 8);
			if (i > 0) {
				//draw one line
				g.drawLine((int)x[i-1].getX(), (int)x[i-1].getY(),
				(int)x[i].getX(), (int)x[i].getY());
			}
			if (i > 1) {
				//draw the other two lines
				g.drawLine((int)x[i-2].getX(), (int)x[i-2].getY(),
				(int)x[i].getX(), (int)x[i].getY());
				//create triangle from points

				//draws answer onto screen

					Triangle tri = new Triangle(x[i-2].getX(), x[i-2].getY(),
					x[i-1].getX(), x[i-1].getY(), x[i].getX(), x[i].getY());
					g.setColor(Color.black);
					g.drawString("Area: "+tri.Area()+"px", 6, 20);
					g.drawString("Perimeter: "+tri.Perimeter()+"px", 6, 40);
					g.setColor(c);


				//resets the program on next click
				i=0;
				x = new Point[0];
			}
		}
	}

}
