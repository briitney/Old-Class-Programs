//Britney Reese, CS 120 03 15f
//This program alphabetizes three strings
import java.io.*;
import java.util.*;

public class Alphabetize {

	private static Scanner input;

	public static void main (String[] args) throws Exception {

		input = new Scanner (System.in);
		System.out.println("Enter three strings: ");
		String x = input.nextLine();
		String y = input.nextLine();
		String z = input.nextLine();

		//Gain user input

		int longest = 1;
		if ( x.length() >= y.length() && x.length() >= z.length() ) {
			longest = x.length();
		} else if ( y.length() >= z.length() && y.length() >= x.length() ) {
			longest = y.length();
		} else if ( z.length() >= y.length() && z.length() >= x.length() ) {
			longest = z.length();
		}

		//Find the length of the longest string

		String a = "";
		a = Format (x, longest);

		String b = "";
		b = Format (y, longest);

		String c = "";
		c = Format (z, longest);

		//Formats strings to create new, corresponding strings that are all lowercase and the same length.

		String first = First(a, b, c, x, y, z);
		String last = Last(a, b, c, x, y, z);
		String middle = Middle(a, b, c, x, y, z);

		//Pass arguments to strings

		System.out.println(first);
		System.out.println(middle);
		System.out.println(last);

		//Prints results


	}

	public static String First (String a, String b, String c, String x, String y, String z) {

	//Compares letter by letter value of strings until one is greatest, returning the string with the highest alphabetical precedence
	//If more than one string has the same value, and those strings are the first, returns the first one entered

		int i = 0;
		while ( true ) {

			if ( a.equals(b) && a.equals(c) && b.equals(c) ) {
				return x;
			}

			if ( a.equals(b) || a.equals(c) || b.equals(c) ) {
				if ( a.equals(b) ) {
					if ( a.charAt(i) < c.charAt(i) ) {
						return x;
					} else {
						if ( c.charAt(i) < a.charAt (i) ) {
							return z;
						}
					}
				} else {
					if ( a.equals(c) ) {
						if ( a.charAt(i) < b.charAt(i) ) {
							return x;
						} else {
							if ( b.charAt(i) < a.charAt (i) ) {
								return y;
							}
						}
					} else {
						if ( b.equals(c) ) {
							if ( a.charAt(i) < b.charAt(i) ) {
								return x;
							} else {
								if ( b.charAt(i) < a.charAt (i) ) {
									return y;
								}
							}
						}
					}
				}
			} else {
				if ( a.charAt(i) < b.charAt(i) && a.charAt(i) < c.charAt(i) ) {
					return x;

				} else {
					if ( b.charAt(i) < a.charAt(i) && b.charAt(i) < c.charAt(i)) {
						return y;

					} else {
						if ( c.charAt(i) < a.charAt(i) && c.charAt(i) < b.charAt(i) ) {
							return z;

						}
					}
				}
			}

			i++;
		}
	}

	public static String Last (String a, String b, String c, String x, String y, String z) {

	//Compares letter by letter value of strings until one is greatest, returning the string with the lowest alphabetical precedence
	//If more than one string has the same value, and those strings are the last, returns the first one entered

		int i = 0;
		while ( true ) {

			if ( a.equals(b) && a.equals(c) && b.equals(c) ) {
				return x;
			}

			if ( a.equals(b) || a.equals(c) || b.equals(c) ) {
				if ( a.equals(b) ) {
					if ( a.charAt(i) > c.charAt(i) ) {
						return x;
					} else {
						if ( c.charAt(i) > a.charAt (i) ) {
							return z;
						}
					}
				} else {
					if ( a.equals(c) ) {
						if ( a.charAt(i) > b.charAt(i) ) {
							return x;
						} else {
							if ( b.charAt(i) > a.charAt (i) ) {
								return y;
							}
						}
					} else {
						if ( b.equals(c) ) {
							if ( a.charAt(i) > b.charAt(i) ) {
								return x;
							} else {
								if ( b.charAt(i) > a.charAt (i) ) {
									return y;
								}
							}
						}
					}
				}
			} else {
				if ( a.charAt(i) > b.charAt(i) && a.charAt(i) > c.charAt(i) ) {
					return x;

				} else {
					if ( b.charAt(i) > a.charAt(i) && b.charAt(i) > c.charAt(i)) {
						return y;

					} else {
						if ( c.charAt(i) > a.charAt(i) && c.charAt(i) > b.charAt(i) ) {
							return z;

						}
					}
				}
			}

			i++;
		}
	}

	public static String Middle (String a, String b, String c, String x, String y, String z) {

	//Compares letter by letter value of strings until one is greatest, returning the string that is not the first or the last
	//If more than one string has the same value, returns the first one entered

		int i = 0;
		while ( true ) {

			if ( a.equals(b) && a.equals(c) && b.equals(c) ) {
				return x;
			}

			if ( a.equals(b) || a.equals(c) || b.equals(c) ) {
				if ( a.equals(b) ) {
					return x;
				} else {
					if ( a.equals(c) ) {
						return x;
					} else {
						if ( b.equals(c) ) {
							return y;
						}
					}
				}
			} else {
				if ( (a.charAt(i) < b.charAt(i) && a.charAt(i) > c.charAt(i)) || (a.charAt(i) > b.charAt(i) && a.charAt(i) < c.charAt(i)) ) {
					return x;

				} else {
					if ( (b.charAt(i) < a.charAt(i) && b.charAt(i) > c.charAt(i)) || (b.charAt(i) > a.charAt(i) && b.charAt(i) < c.charAt(i)) ) {
						return y;

					} else {
						if ( (c.charAt(i) < a.charAt(i) && c.charAt(i) > b.charAt(i)) || (c.charAt(i) > a.charAt(i) && c.charAt(i) < b.charAt(i))  ) {
							return z;

						}
					}
				}
			}

			i++;
		}
	}

	public static String Format (String a, int longest) {

		//Returns a string with all lowercase letters
		//that also has the same length of the longest
		//of the strings used in main.

		String z = "";

				for ( int i = 0; i < a.length(); i++) {
					if (a.charAt(i) >= 65 && a.charAt(i) <= 90) {
						z = z + (char)(a.charAt(i) + 32) ;
					} else {
						z = z + a.charAt(i);
					}

				}

				for ( int i = z.length(); i < longest; i++ ) {

					z = z + (char) (00);

				}

		return (z);
	}

}