//Britney Reese, CS 120 03 15f
//This program, when given the lenghts of two of the sides of a triangle in inches, and the angle in between them in degrees, prints out the length of the largest side

import java.io.*;
import java.util.*;

public class BigSide {

	public static Scanner in;

	public static void main (String[] args) throws Exception {

		Scanner input = new Scanner (System.in);

		System.out.println ("Enter the length of one side in inches: ");
		double a = input.nextDouble ();
		System.out.println ("Enter the length of another side in inches: ");
		double b = input.nextDouble ();
		System.out.println ("Enter the angle between them in degrees: ");
		double angle = input.nextDouble ();

		double c = Math.sqrt (a*a+b*b-2*a*b*Math.cos(angle*Math.PI/180));

		double Biggest = 0;

		if ( a > b ) {

			if ( a > c ) {

				Biggest = a;

				} else {

					Biggest = c;

					}

			} else {

				if ( b > c ) {

					Biggest = b;

					} else {

						Biggest = c;

						}

				}

		System.out.println ("The length of the largest side is " + Biggest + " inches.");

		}

	}