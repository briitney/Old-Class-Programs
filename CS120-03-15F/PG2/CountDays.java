//Britney Reese CS 120 03 15f
//This program computes the total number of days when given the start and end years.

import java.io.*;
import java.util.*;

public class CountDays {

	public static Scanner in;

	public static void main (String[] args) throws Exception {

		Scanner input = new Scanner (System.in);

		System.out.println ("Please enter the starting year: ");
		int year1 = input.nextInt ();
		System.out.print ("Please enter the ending year: ");
		int year2 = input.nextInt ();
		int days = 0;

		for (; year1 <= year2; year1 = year1+1) {

			if (year1%4 == 0) {

				days = days + 366;

			} else {

				days = days + 365;

			}

		}

		System.out.println ("The number of days total is " + days + ".");

	}

}