//Britney Reese CS 120-01-15f
//This is a class for a point
import java.awt.*;
import java.applet.*;
import java.awt.event.*;
import java.awt.Image;

public class PacMan extends Applet {

	//PacMan is a point, I wish I would have made a seperate circle class but too late now
	private Point Pac = new Point(15,180);
	//This square lights up when Pacman runs into it
	private Maze mistake = new Maze(-50, -50);
	//The dots are stored in an array of points
	private Point[] Dotz = new Point[0];
	//These booleans keep track of pacman's orientation
	private boolean up, down, left, right;
	//These are all various counters
	private int dPac, degree, speed, start;
	//This is an array that stores the loaction of maze peices
	private int[] MazeMatrix = {   0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0,
								   0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
								   0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
								   0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0,
								   0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0,
								   1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
								   1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
								   0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0,
								   0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0,
								   0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
								   0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
								   0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0 };
	//This is the array that will hold the maze peices
	private Maze[] Mazes = new Maze[MazeMatrix.length];

	public void init () {
		resize(360,360);
		setBackground(Color.black);
		dPac = 0;
		speed = 7;
		start = 1;
		int x = 0;
		int y = 0;
		//this loop runs through the array of locations and makes maze peices
		//in the second array
		for (int i = 0; i < MazeMatrix.length; i++) {
			if (MazeMatrix[i]==0) {
				Mazes[i]= new Maze(x*30, y*30);
			} else {
				Mazes[i]= new Maze(-100, -100);
			}
			x++;
			if (x == 12){
				y++;
				x = 0;
			}
		}

		addMouseListener(new MouseAdapter () {
			public void mousePressed (MouseEvent e) {
				//This is used to add a dot if the game has been started
				if (start == 3) {
					Point[] Dotzz = new Point[Dotz.length+1];
					for (int i = 0; i < Dotz.length; i ++) Dotzz[i] = Dotz[i];
					Dotzz[Dotz.length] = new Point(e.getX(),e.getY());
					Dotz = Dotzz;
				}
				start = 3;
				repaint();
			}
		});
		addKeyListener(new KeyAdapter () {
			public void keyPressed (KeyEvent e) {
				//These four ifs tests if the arrows have been pressed
				if (e.getKeyCode()==KeyEvent.VK_UP) {
					up = true;
					down = false;
					left = false;
					right = false;
					dPac = dPac + 1;
					//This tests if Pacman hit a wall
					boolean hitTheWall = false;
					for (int i = 1; i < MazeMatrix.length; i++) {
						if (Mazes[i].CollideUp(Pac) == true) {
							hitTheWall = true;
							mistake = Mazes[i];
						}
					}
					if (hitTheWall == false) Pac.setY(Pac.getY()-speed);
				}
				if (e.getKeyCode()==KeyEvent.VK_DOWN) {
					up = false;
					down = true;
					left = false;
					right = false;
					dPac = dPac + 1;
					boolean hitTheWall = false;
					for (int i = 1; i < MazeMatrix.length; i++) {
						if (Mazes[i].CollideDown(Pac) == true) {
							hitTheWall = true;
							mistake = Mazes[i];
						}
					}
					if (hitTheWall == false) Pac.setY(Pac.getY()+speed);
				}
				if (e.getKeyCode()==KeyEvent.VK_LEFT) {
					up = false;
					down = false;
					left = true;
					right = false;
					dPac = dPac + 1;
					boolean hitTheWall = false;
					for (int i = 1; i < MazeMatrix.length; i++) {
						if (Mazes[i].CollideLeft(Pac) == true) {
							hitTheWall = true;
							mistake = Mazes[i];
						}
					}
					if (hitTheWall == false) Pac.setX(Pac.getX()-speed);
				}
				if (e.getKeyCode()==KeyEvent.VK_RIGHT) {
					up = false;
					down = false;
					left = false;
					right = true;
					dPac = dPac + 1;
					boolean hitTheWall = false;
					for (int i = 1; i < MazeMatrix.length; i++) {
						if (Mazes[i].CollideRight(Pac) == true) {
							hitTheWall = true;
							mistake = Mazes[i];
						}
					}
					if (hitTheWall == false) Pac.setX(Pac.getX()+speed);
				}
				//This loops the screan around if Pacman runs off the edge
				if (Pac.getX() > 360) Pac.setX(0);
				if (Pac.getY() > 360) Pac.setY(0);
				if (Pac.getX() < 0) Pac.setX(360);
				if (Pac.getY() < 0) Pac.setY(360);
				//This finds if pacman ate a dot and removes it
				Dotz = Eat(Dotz, Pac);
				repaint();
			}
		});
	}

	public void paint (Graphics g) {
		g.setColor(Color.white);
		degree = dPac*9;
		//Start Screen
		if (start == 1) {
			Image StartScreen = getImage(getCodeBase(), "Background.png");
			//g.drawString("*Click to Start!*", 270, 280);
			g.drawImage(StartScreen, 0,0,360,360,this);
		}
		//Paints Dots
		for (int i=0; i < Dotz.length; i++)
			g.fillOval ((int)Dotz[i].getX()-5,(int)Dotz[i].getY()-5,10,10);
		g.setColor(Color.yellow);
		//Paints Pacman on the first frame, before
		//The mouse is ever pressed
		if (start == 3 && !up && !down && ! left && !right) {
			g.fillArc((int)Pac.getX()-(50/2), (int)Pac.getY()-(50/2), 50, 50, 45-(int)degree, 270);
			g.fillArc((int)Pac.getX()-(50/2), (int)Pac.getY()-(50/2), 50, 50, 180, 180-(45-(int)degree));
		}
		//Paint the maze
		g.setColor(Color.blue);
		if (start == 3)
			for (int i = 0; i < MazeMatrix.length; i++) {
				g.fillRect((int)Mazes[i].getX(), (int)Mazes[i].getY(), 30, 30);
			}
		//Paint mistake block
		g.setColor(Color.red);
		g.fillRect((int)mistake.getX(), (int)mistake.getY(), 30, 30);
		g.setColor(Color .yellow);
		//I could have used a method for this, all it does if paint pacman
		//Using information like mouth opening degree and x and y coordinates
		if (up){
			if (dPac <= 5) {
				g.fillArc((int)Pac.getX()-(50/2), (int)Pac.getY()-(50/2), 50, 50, 45-(int)degree+90, 270);
				g.fillArc((int)Pac.getX()-(50/2), (int)Pac.getY()-(50/2), 50, 50, 180+90, 180-(45-(int)degree));
			}
			if (dPac >= 5) {
				g.fillArc((int)Pac.getX()-(50/2), (int)Pac.getY()-(50/2), 50, 50, (int)degree-45+90, 200);
				g.fillArc((int)Pac.getX()-(50/2), (int)Pac.getY()-(50/2), 50, 50, 180+90, 235-(int)degree);
			}
		}
		if (down){
			if (dPac <= 5) {
				g.fillArc((int)Pac.getX()-(50/2), (int)Pac.getY()-(50/2), 50, 50, 45-(int)degree+270, 270);
				g.fillArc((int)Pac.getX()-(50/2), (int)Pac.getY()-(50/2), 50, 50, 180+270, 180-(45-(int)degree));
			}
			if (dPac >= 5) {
				g.fillArc((int)Pac.getX()-(50/2), (int)Pac.getY()-(50/2), 50, 50, (int)degree-45+270, 200);
				g.fillArc((int)Pac.getX()-(50/2), (int)Pac.getY()-(50/2), 50, 50, 180+270, 235-(int)degree);
			}
		}
		if (left){
			if (dPac <= 5) {
				g.fillArc((int)Pac.getX()-(50/2), (int)Pac.getY()-(50/2), 50, 50, 45-(int)degree+180, 270);
				g.fillArc((int)Pac.getX()-(50/2), (int)Pac.getY()-(50/2), 50, 50, 180+180, 180-(45-(int)degree));
			}
			if (dPac >= 5) {
				g.fillArc((int)Pac.getX()-(50/2), (int)Pac.getY()-(50/2), 50, 50, (int)degree-45+180, 200);
				g.fillArc((int)Pac.getX()-(50/2), (int)Pac.getY()-(50/2), 50, 50, 180+180, 235-(int)degree);
			}
		}
		if (right){
			if (dPac <= 5) {
				g.fillArc((int)Pac.getX()-(50/2), (int)Pac.getY()-(50/2), 50, 50, 45-(int)degree, 270);
				g.fillArc((int)Pac.getX()-(50/2), (int)Pac.getY()-(50/2), 50, 50, 180, 180-(45-(int)degree));
			}
			if (dPac >= 5) {
				g.fillArc((int)Pac.getX()-(50/2), (int)Pac.getY()-(50/2), 50, 50, (int)degree-45, 200);
				g.fillArc((int)Pac.getX()-(50/2), (int)Pac.getY()-(50/2), 50, 50, 180, 235-(int)degree);
			}
		}
		//resets the dPac counter(what frame Pacman is on)
		if (dPac > 10) dPac = 0;

	}

	//Tests if the dot has been eaten by pacman
	public Point[] Eat (Point[] a, Point pac) {
		Point[] okay = new Point[a.length];
		for (int i = 0; i < a.length; i++) {
			if (Math.pow(pac.getX()-a[i].getX(),2)+Math.pow(pac.getY()-a[i].getY(),2) < Math.pow((50+10)/2,2)) {
				okay[i]= new Point(-100,-100);
			} else {
				okay[i] = a[i];
			}
		}
		return okay;
	}

}