//Britney Reese CS 120-01-15f
//This is a class for a block of the maze
import java.io.*;
import java.util.*;

public class Maze {

	private double x, y;

	public Maze (double X, double Y) {
		x = X;
		y = Y;
	}

	public double getX () {return x;}
	public double getY () {return y;}

	//This method finds if Pacman collides with the maze from the right
	public boolean CollideRight (Point paku) {
		if (paku.getY()>y-25 && paku.getY()<(y+30)+25){
			if (Math.abs(x-paku.getX())<25) {
				return true;
			}
		}
		return false;
	}

	//This method finds if Pacman collides with the maze from the left
	public boolean CollideLeft (Point paku) {
		if (paku.getY()>y-25 && paku.getY()<(y+30)+25){
			if (Math.abs(x+30-paku.getX())<25) {
				return true;
			}
		}
		return false;
	}

	//This method finds if Pacman collides with the maze frome the bottom
	public boolean CollideDown (Point paku) {
		if (paku.getX()>x-25 && paku.getX()<(x+30)+25){
			if (Math.abs(y-paku.getY())<25) {
				return true;
			}
		}
		return false;
	}

	//This method finds if Pacman collides with the maze from the top
	public boolean CollideUp (Point paku) {
		if (paku.getX()>x-25 && paku.getX()<(x+30)+25){
			if (Math.abs(y+30-paku.getY())<25) {
				return true;
			}
		}
		return false;
	}


}