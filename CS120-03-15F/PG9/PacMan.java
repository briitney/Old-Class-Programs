//Britney Reese CS 120-01-15f
//This is a program for a Pacman that goes around the screen
import java.awt.*;
import java.applet.*;
import java.awt.event.*;

public class PacMan extends Applet {

	//Declaring variables
	private Point Pac = new Point(50,450);
	private Point[] Dotz = new Point[1];
	private boolean up, down, left, right;
	String text= "";
	int i, dPac, degree, speed, size, start;

	//Init method
	public void init () {
		//Assigns values to variables
		resize(600,600);
		setBackground(Color.black);
		dPac = 0;
		speed = 18;
		size = 50;
		start = 1;
		//Starts game on click
		addMouseListener(new MouseAdapter () {
			public void mousePressed (MouseEvent e) {
				start = 3;
				repaint();
			}
		});
		//Controls the keyboard
		addKeyListener(new KeyAdapter () {
			public void keyPressed (KeyEvent e) {
				if (e.getKeyCode()==KeyEvent.VK_UP) {
					//Detects if the key is up and orrients pacman appropriately
					up = true;
					down = false;
					left = false;
					right = false;
					dPac = dPac + 1;
					//Moves Pacman
					Pac.setY(Pac.getY()-speed);
				}
				if (e.getKeyCode()==KeyEvent.VK_DOWN) {
					up = false;
					down = true;
					left = false;
					right = false;
					dPac = dPac + 1;
					Pac.setY(Pac.getY()+speed);
				}
				if (e.getKeyCode()==KeyEvent.VK_LEFT) {
					up = false;
					down = false;
					left = true;
					right = false;
					dPac = dPac + 1;
					Pac.setX(Pac.getX()-speed);
				}
				if (e.getKeyCode()==KeyEvent.VK_RIGHT) {
					up = false;
					down = false;
					left = false;
					right = true;
					dPac = dPac + 1;
					Pac.setX(Pac.getX()+speed);
				}
				//Wraps the screen around
				if (Pac.getX() > 600) Pac.setX(0);
				if (Pac.getY() > 600) Pac.setY(0);
				if (Pac.getX() < 0) Pac.setX(600);
				if (Pac.getY() < 0) Pac.setY(600);
				repaint();
			}
		});
	}

	//Method for painting
	public void paint (Graphics g) {
		g.setColor(Color.yellow);
		degree = dPac*9;
		//Draws screen if no clicks
		if (start == 1) {
			g.drawString("*Click to Start!*", 270, 280);
		}
		//Draws Pacman if one click, but no keys pressed
		if (start == 3 && !up && !down && !left && !right) {
			g.fillArc((int)Pac.getX()-(size/2), (int)Pac.getY()-(size/2), size, size, 45-(int)degree, 270);
			g.fillArc((int)Pac.getX()-(size/2), (int)Pac.getY()-(size/2), size, size, 180, 180-(45-(int)degree));

		}
		//Draws pacmanin the propper orientation
		if (up){
			if (dPac <= 5) {
				g.fillArc((int)Pac.getX()-(size/2), (int)Pac.getY()-(size/2), size, size, 45-(int)degree+90, 270);
				g.fillArc((int)Pac.getX()-(size/2), (int)Pac.getY()-(size/2), size, size, 180+90, 180-(45-(int)degree));
			}
			if (dPac >= 5) {
				g.fillArc((int)Pac.getX()-(size/2), (int)Pac.getY()-(size/2), size, size, (int)degree-45+90, 200);
				g.fillArc((int)Pac.getX()-(size/2), (int)Pac.getY()-(size/2), size, size, 180+90, 235-(int)degree);
			}
		}
		if (down){
			if (dPac <= 5) {
				g.fillArc((int)Pac.getX()-(size/2), (int)Pac.getY()-(size/2), size, size, 45-(int)degree+270, 270);
				g.fillArc((int)Pac.getX()-(size/2), (int)Pac.getY()-(size/2), size, size, 180+270, 180-(45-(int)degree));
			}
			if (dPac >= 5) {
				g.fillArc((int)Pac.getX()-(size/2), (int)Pac.getY()-(size/2), size, size, (int)degree-45+270, 200);
				g.fillArc((int)Pac.getX()-(size/2), (int)Pac.getY()-(size/2), size, size, 180+270, 235-(int)degree);
			}
		}
		if (left){
			if (dPac <= 5) {
				g.fillArc((int)Pac.getX()-(size/2), (int)Pac.getY()-(size/2), size, size, 45-(int)degree+180, 270);
				g.fillArc((int)Pac.getX()-(size/2), (int)Pac.getY()-(size/2), size, size, 180+180, 180-(45-(int)degree));
			}
			if (dPac >= 5) {
				g.fillArc((int)Pac.getX()-(size/2), (int)Pac.getY()-(size/2), size, size, (int)degree-45+180, 200);
				g.fillArc((int)Pac.getX()-(size/2), (int)Pac.getY()-(size/2), size, size, 180+180, 235-(int)degree);
			}
		}
		if (right){
			if (dPac <= 5) {
				g.fillArc((int)Pac.getX()-(size/2), (int)Pac.getY()-(size/2), size, size, 45-(int)degree, 270);
				g.fillArc((int)Pac.getX()-(size/2), (int)Pac.getY()-(size/2), size, size, 180, 180-(45-(int)degree));
			}
			if (dPac >= 5) {
				g.fillArc((int)Pac.getX()-(size/2), (int)Pac.getY()-(size/2), size, size, (int)degree-45, 200);
				g.fillArc((int)Pac.getX()-(size/2), (int)Pac.getY()-(size/2), size, size, 180, 235-(int)degree);
			}
		}
		//resets the oppening and closing of pacman's mouth
		if (dPac > 10) dPac = 0;

	}
}