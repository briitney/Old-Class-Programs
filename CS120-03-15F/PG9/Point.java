//Britney Reese CS 120-01-15f
//This is a class for a point
import java.io.*;
import java.util.*;

public class Point {

	private double x, y;

	//Setter method
	public Point (double a, double b) {
		x = a;
		y = b;
	}

	//Getter method
	public double getX () {
		return x;
	}

	//Getter method
	public double getY () {
		return y;
	}

	//Setter method
	public void setX ( double a) {
		x = a;
	}

	//Setter method
	public void setY ( double a) {
		y = a;
	}

	//Calculates distance
	public double Distance (Point B) {
		return Math.pow(Math.pow(x-B.getX(),2)+Math.pow(y-B.getY(),2),0.5);
	}

}