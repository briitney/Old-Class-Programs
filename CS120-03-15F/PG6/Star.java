//Britney Reese, CS 120-03-15f
//This program draws a pretty picture that consists of several Stars of David.

import java.applet.*;
import java.awt.*;

public class Star extends Applet {

	//Here is where I initialize variables and arrays I want to use in various methods
	int[] blue, x, y, width;
	int w;
	int num;
	int percent;

	public void init () {

		resize (800,800);
		setBackground (Color.black);

		//Here is where I assign values to the variables
		//num is number of stars, percent is the percent of stars that are outlines,
		//blue is used in determining the shade of each star, x and y are the coordinates of
		//each star staring in the upper right of each drawing, and width is the width of
		//each drawing.
		num = 100;
		//percent is randomly between 50-100%
		percent = 50+(int)(50*Math.random());
		blue = new int[num];
		x = new int[num];
		y = new int[num];
		width = new int[num];

		//for initializing arrays of random variables
		for (int i = 0; i < num; i++) {
			blue[i] = (int)(256*Math.random());
			x[i] = (int)(800*Math.random()-75);
			y[i] = (int)(800*Math.random()-75);
			//size is random between 50-150px
			width[i] = (int)(100*Math.random())+50;
		}

	}

	public void paint ( Graphics g ) {

		for (int i = 0; i < num; i++) {
			//where methods and variables that are specific to graphics are looped through
			g.setColor (new Color (255,255,blue[i]));
			w = width[i];
			if (i < (int)(percent*num/100)) DrawStarOfDavid( g, x[i], y[i]);
			else DrawSolidStarOfDavid( g, x[i], y[i]);
		}

	}

	public void DrawStarOfDavid (Graphics g, int x, int y) {

		//This method draws the outline of a Star of David line by line

		g.drawLine(x+(int)(w/2.0), y, x+w, y+(int)(3.0*w/4.0));
		g.drawLine(x+w, y+(int)(3.0*w/4.0), x, y+(int)(3.0*w/4.0));
		g.drawLine(x, y+(int)(3.0*w/4.0), x+(int)(w/2.0), y);

		g.drawLine(x+(int)(w/2.0), y+w, x+w, y+(int)(w/4.0));
		g.drawLine(x+w, y+(int)(w/4.0), x, y+(int)(w/4.0));
		g.drawLine(x, y+(int)(w/4.0), x+(int)(w/2.0), y+w);

	}

	public void DrawSolidStarOfDavid (Graphics g, int x, int y) {

		//This method draws a Star of David by drawing two
		//triangles and filling them in

		int[] a = {x+(int)(w/2.0), x+w, x};
		int[] b = {y, y+(int)(3.0*w/4.0), y+(int)(3.0*w/4.0)};
		int[] c = {x+(int)(w/2.0), x+w, x};
		int[] d = {y+w, y+(int)(w/4.0), y+(int)(w/4.0)};

		Polygon upTriangle = new Polygon(a,b,3);
		Polygon downTriangle = new Polygon(c,d,3);
		g.fillPolygon(upTriangle);
		g.fillPolygon(downTriangle);

	}

}