//Britney Reese CS 120 03 15f
//This program prints out the sum of square roots


import java.io.*;
import java.util.*;

public class Sqrts {

	private static Scanner input;

	public static void main (String[] args) throws Exception {
		int a = 1;
		Boolean test = true;
		System.out.println("Please enter a postive integer: ");
		input = new Scanner (System.in);
		while (test == true) {
			a = input.nextInt();
			if ( a <= 0 )
				System.out.println("That isn't a positive integer. Try again.");
			else if ( a > 0 )
				test = false;
		}
		double tog = 0, b;
		for (; a>0 ; a-=1) {
			b = Math.pow(a,0.5);
			tog = tog + b;
		}
		System.out.println("The sum of the square roots is "+tog+".");

	}

}