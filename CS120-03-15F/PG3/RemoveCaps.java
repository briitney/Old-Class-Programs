//Britney Reese CS 120 03 15f
//This program appears to remove the capital letters from a string
import java.io.*;
import java.util.*;

public class RemoveCaps {

	private static Scanner input;

	public static void main (String[] args) throws Exception {

		System.out.println("Enter a string: ");
		input = new Scanner (System.in);
		String text = input.nextLine();
		System.out.println("Removing caps...");
		String b = "";

		for ( int i = 0; i < text.length(); i++) {
			if (text.charAt(i) < 65 || text.charAt(i) > 90) {
				b = b + text.charAt(i);
			}

		}

		System.out.println(b);

	}

}
