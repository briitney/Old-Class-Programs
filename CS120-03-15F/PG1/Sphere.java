//Britney Reese, CS 120 03 15f
//This program asks for the radius of a sphere, and outputs the Volume and Surface Area of the sphere.

import java.io.*;
import java.util.*;

public class Sphere
{
	private static Scanner in;

	public static void main(String[] args)
	{
		double radius;
		double volume;
		double area;

		System.out.print( "Enter the radius of the sphere in inches: ");
		Scanner input = new Scanner(System.in);
   		radius = input.nextDouble();
		volume = Math.pow(radius,3) * Math.PI * 4.0/3.0;
		area = 4 * Math.pow(radius,2) * Math.PI;
		System.out.println( "The volume of the sphere is " + volume + " cubic inches." );
		System.out.println( "The surface area of the sphere is " + area + " square inches." );
	}
}