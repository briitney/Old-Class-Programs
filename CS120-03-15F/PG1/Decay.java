//Britney Reese, CS 120 03 15f
//This program computes the expotential decay substance, with the input being the initial mass, and the outputs being the mass after so many years.

import java.io.*;
import java.util.*;

public class Decay
{
	private static Scanner in;

	public static void main(String[] args)
	{
		double initial;
		System.out.print( "Enter the initial mass of the substance in grams: " );
		Scanner input = new Scanner(System.in);
		initial = input.nextDouble();

		for (int c = 1; c < 11; c = c+1) {
			initial = initial * 0.97;

			String a = "";
			if (c == 1) {
				a = "one";
			} else if (c == 2) {
				a = "two";
			} else if (c == 3) {
				a = "three";
			} else if (c == 4) {
				a = "four";
			} else if (c == 5) {
				a = "five";
			} else if (c == 6) {
				a = "six";
			} else if (c == 7) {
				a = "seven";
			} else if (c == 8) {
				a = "eight";
			} else if (c == 9) {
				a = "nine";
			} else if (c == 10) {
				a = "ten";
			}
			System.out.print( "After " + a + " years, there are " + initial + " grams.\n");
		}
	}
}