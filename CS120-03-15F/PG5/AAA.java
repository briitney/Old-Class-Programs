//Britney Reese, CS 120 03 15f
//This program accepts strings, and if the strings contain two 'a's
//with a character in between them, returns the strings in the reverse order they were entered.

import java.io.*;
import java.util.*;

public class AAA {

	private static Scanner input;

	public static void main (String[] args) throws Exception {

		//Main method

		input = new Scanner (System.in);

		int num = wordNum();

		String[] Inputs = words(num);

		findAndPrint(num, Inputs);

	}

	public static int wordNum() {

		//This determines the number of words Inputs is going to contain
		//Also makes sure that the number is a positive integer

		int num = 0;
		while (true) {
			try {
				System.out.println("How many words do you want to enter?");
				num = Integer.parseInt(input.nextLine());
				if ( num > 0 ) break;
				else System.out.println("That's not a positive integer.");
			} catch (Exception e) {
				System.out.println("That's not a positive integer");
			}
		}
		return num;

	}

	public static String[] words (int num) {

		//Creates Inputs and inputs Strings into it

		String[] Inputs = new String[num];
		for (int i = 0; i < num; i++) {
			System.out.println("Enter word "+(i+1)+": ");
			Inputs[i] = input.nextLine();
		}
		return Inputs;


	}

	public static void findAndPrint (int num, String[] Inputs) {

		//Prints out words that have two 'a's separated by a character

		System.out.println("The AAA words are: ");
		int count = 0;
		for (int i = num - 1; i >= 0; i-= 1, count = 0) {
			for (int j = 0; j < Inputs[i].length() - 2; j++) {
				if (Inputs[i].charAt(j) == 'a' ||
					Inputs[i].charAt(j) == 'A') {
					if (Inputs[i].charAt(j+2) == 'a' ||
						Inputs[i].charAt(j+2) == 'A') {
						System.out.println(Inputs[i]);
						break;
					}
				}
			}
		}


	}

}