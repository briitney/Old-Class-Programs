//Britney Reese, CS 201 01 16f
//This program checks if a checkers move is legal or not

#include "Checkers.h"

//This is the main method of the program
int main(int argc, char **argv) {
	cout << "How many rows does your checkerboard have? ";
	int rows;
	cin >> rows;
	cout << "How many columns does your checkerboard have? ";
	int cols;
	cin >> cols;
	char **board = new char*[rows];
	cin.ignore();
	//Create the checkerboard
	cout << "Enter your checkerboard below: " << endl;
	for (int i = 0; i < rows; i++) {
		string a;
		getline(cin, a);
		board[i] = new char[cols];
		addRow(i, cols, a, board);
	}
	int isit = 0;
	cout << "Enter row to be checked: ";
	int checkRow;
	cin >> checkRow;
	cout << "Enter column to be checked: ";
	int checkCol;
	cin >> checkCol;
	cin.ignore();
	//Check the checkerboard, passing around too many variables
	//Run once for each driection
	//isit refers to if there is a legal move
	isit = isit + CheckCheck(-1, -1, isit, board, rows, cols, checkRow, checkCol);
	isit = isit + CheckCheck(-1, 1, isit, board, rows, cols, checkRow, checkCol);
	isit = isit + CheckCheck(1, 1, isit, board, rows, cols, checkRow, checkCol);
	isit = isit + CheckCheck(1, -1, isit, board, rows, cols, checkRow, checkCol);
	if (isit < 1) cout << "The checker at this square can not make a valid jump.";
	else cout << "The checker at this square can make a valid jump.";
	for (int i = 0; i < rows; i++) {
		delete[] board[i];
	}
	delete[] board;
	return 0;
}

void addRow(int rowNum, int cols, string a, char **board) {
	//Reads in the input and adds each in as a char of the board array
	for (int i = 0; i < cols; i++)
		board[rowNum][i] = a[i];
}

string returnRow(int rowNum, int cols, char **board) {
	//Used for debugging
	string a = "";
	for (int i = 0; i < cols; i++)
		a = a + board[rowNum][i];
	return a;
}

//Logic of the program, does the actual checker checking
int CheckCheck (int a, int b, int isit, char **board, int rows, int cols, int checkRow, int checkCol) {
	//If the board is in bounds
	if (checkRow+2*a >=0 && checkRow+2*a <= rows && checkCol+2*b >= 0 && checkCol+2*b <= cols) {
		//If the adjacent checker in that direction is of the opposite color
		if (board[checkRow][checkCol] != board[checkRow+a][checkCol+b] && board[checkRow+a][checkCol+b] != ' ') {
			//If there is a free space
			if (board[checkRow+2*a][checkCol+2*b] == ' ') { return 1; }
		}
	}
	return 0;
}
