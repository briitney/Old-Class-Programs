//Britney Reese, CS 201 01 16f
//Header file for the Checkers Program
#ifndef _CHECKERS_
#define _CHECKERS_

#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;

int main(int argc, char **argv);
void addRow(int rowNum, int cols, string a, char **board);
string returnRow(int rowNum, int cols, char **board);
int CheckCheck (int a, int b, int isit, char **board, int rows, int cols, int checkRow, int checkCol);

#endif
