/*
 * Card.cpp
 *
 *  Created on: Sep 12, 2016
 *      Author: Britney Reese
 */

//This code creates the Card class

#include <iostream>
#include <string>
#include <cstdlib>
#include "Card.h"
using namespace std;

Card::Card (int v) {
	value = v;
	rank = v / 4;
	suit = v % 4;
	setName();
}

void Card::setName() {
	switch (rank) {
		case 0: name = "King"; break;
		case 1: name = "Ace"; break;
		case 2: name = "Two"; break;
		case 3: name = "Three"; break;
		case 4: name = "Four"; break;
		case 5: name = "Five"; break;
		case 6: name = "Six"; break;
		case 7: name = "Seven"; break;
		case 8: name = "Eight"; break;
		case 9: name = "Nine"; break;
		case 10: name = "Ten"; break;
		case 11: name = "Jack"; break;
		case 12: name = "Queen"; break;
	}

	name += " of ";

	switch (suit) {
		case 0: name += "Clubs"; break;
		case 1: name += "Diamonds"; break;
		case 2: name += "Hearts"; break;
		case 3: name += "Spades"; break;
	}
}

string Card::getName() {
	return name;
}

int Card::getValue() {
	return value;
}

int Card::getRank() {
	return rank;
}

int Card::getSuit() {
	return suit;
}
