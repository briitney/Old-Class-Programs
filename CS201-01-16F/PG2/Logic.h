/*
 * Logic.h
 *
 *  Created on: Sep 13, 2016
 *      Author: Britney Reese
 */

#ifndef LOGIC_H_
#define LOGIC_H_

#include <iostream>
#include <string>
#include <cstdlib>
#include "Deck.h"
using namespace std;

class Logic {
private:
	//Keeps track of best hand
	int r;
	//Makes the deck a class variable
	Deck* deck;
	//Used as conditions when checking
	bool sameSuit;
	bool straight;

public:
	//inits, runs methods, and kills logic
	Logic(Deck* dk);
	//init class bools
	void initSameSuit();
	void initStraight();
	//prints the results
	void printResult();
	//checks varios hands
	void checkRoyalFlush();
	void checkStraightFlush();
	void checkOfAKind();
	void checkFlushStraight();
	void checkHigh();
	//compares r and a number and assigns r to the lowest
	void compR(int num);
};

#endif /* LOGIC_H_ */
