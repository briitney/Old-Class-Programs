/*
 * Card.h
 *
 *  Created on: Sep 12, 2016
 *      Author: Britney Reese
 */

//Header file for Card.cpp

#ifndef CARD_H_
#define CARD_H_

#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;

int main ();

class Card {
private:
	//Simply holds values
	int value;
	int rank;
	int suit;
	string name;

public:
	//Creates class
	Card(int v);
	//Uses the value to set rank, suit, and name
	void setName();
	//Getters
	int getValue();
	int getRank();
	int getSuit();
	string getName();
};

#endif /* CARD_H_ */
