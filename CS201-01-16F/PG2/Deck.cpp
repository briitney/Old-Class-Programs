/*
 * Deck.cpp
 *
 *  Created on: Sep 12, 2016
 *      Author: Britney Reese
 */

//This code creates the Deck class that holds and does functions on an array of Cards.

#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;
#include "Card.h"
#include "Deck.h"

Deck::Deck (int size) {
	sz = size;
	Cards = new Card* [sz];
}

void Deck::addCards () {
	for (int i = 0; i < sz; i++) {
		int val;
		cout << "Enter card " << i+1 << ": ";
		cin >> val;
		cin.ignore();
		Cards[i] = new Card(val);
	}
}

void Deck::addCard (int num, int val) {
	Cards[num] = new Card(val);
}

void Deck::shuffleDeck () {
	//Shuffle algorithm given in class
	for (int i = 1; i < sz; i++) {
		int r = rand() % (i+1);
		Card *A = Cards[i];
		Cards[i] = Cards[r];
		Cards[r] = A;
	}
}

void Deck::organizeDeck () {
	//Simple unoptimized bubble sort
	for (int i = 0; i < sz-1; i++) {
		for (int j = 0; j < sz-1-i; j++) {
			if (Cards[j]->getValue() > Cards[j+1]->getValue()) {
				Card* A = Cards[j+1];
				Cards[j+1] = Cards[j];
				Cards[j] = A;
			}
		}
	}
}

Card ** Deck::getDeck () {
	return Cards;
}

void Deck::printDeck () {
	cout << endl;
	for (int i = 0; i < sz; i++) {
		cout << Cards[i]->getName() << endl;
	}
	cout << endl;
}

Card * Deck::getCard (int cd) {
	return Cards[cd];
}

int Deck::getSize () {
	return sz;
}

Deck::~Deck() {
	//Cycle through cards and deletes them, the delete the array that was holding the cards.
	for (int i = 0; i < sz; i++) {
		delete Cards[i];
	}
	delete[] Cards;
}
