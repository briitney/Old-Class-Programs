/*
 * Logic.cpp
 *
 *  Created on: Sep 13, 2016
 *      Author: Britney Reese
 */

//Logic engine of sorts, determens what the hand is of an oraganized Deck of size 5

#include <iostream>
#include <string>
#include <cstdlib>
#include "Deck.h"
#include "Logic.h"
using namespace std;

Logic::Logic (Deck* dk) {
	deck = dk;
	//organizes the deck so that logic can use it
	deck->organizeDeck();
	//init to a number that is too high, and reduce when a hand or a better hand is found
	r = 100;
	//init class variables
	initStraight();
	initSameSuit();
	//checks each type of hand
	checkRoyalFlush();
	checkRoyalFlush();
	checkStraightFlush();
	checkOfAKind();
	checkFlushStraight();
	checkHigh();
	//print out the results
	printResult();
	//commit suicide
	delete this;
}

void Logic::initSameSuit() {
	//used to check flushes, goes through and makes sure the other four cards match the suit of the first card
	sameSuit = true;
	int suit = deck->getCard(0)->getSuit();
	for (int i = 1; i < 5; i++) {
		if (deck->getCard(i)->getSuit() != suit) {
			sameSuit = false;
			break;
		}
	}
}

void Logic::initStraight() {
	//checks for straights, being warry that king is 0, not 13
	int first = deck->getCard(0)->getRank();
	//Used to take care of the kooky king
	bool endWithKing = false;
	if (first == 0) {
		if (deck->getCard(1)->getRank() == 9) {
			endWithKing = true;
			first = deck->getCard(1)->getRank()-1;
		} else  {
			straight = false; return;
		}
	}
	//Makes sure the ranks of cards are in order, incrementing by one
	straight =
		deck->getCard(1)->getRank() == first+1 &&
		deck->getCard(2)->getRank() == first+2 &&
		deck->getCard(3)->getRank() == first+3;
	if (endWithKing == false) {
		straight = straight && deck->getCard(4)->getRank() == first+4;
	}
}

void Logic::checkRoyalFlush() {
	//A royal flush only contains certain cards of the same suit
	if (sameSuit == true &&
			deck->getCard(0)->getRank() == 0 &&
			deck->getCard(1)->getRank() == 1 &&
			deck->getCard(2)->getRank() == 10 &&
			deck->getCard(3)->getRank() == 11 &&
			deck->getCard(4)->getRank() == 12) {
		compR(0);
	}
}

void Logic::checkStraightFlush() {
	if (sameSuit == true && straight == true) {
		compR(1);
	}
}

void Logic::checkOfAKind() {
	//checks X of a kind, full house, two pair, and pair.
	//since there are only five cards, there can be at most 2 ranks that have matches, so only need count1 and count2
	int count1 = 0;
	int count2 = 0;
	int count = 0;
	bool countRankSet = false;
	//countRankSet checks if there has been a match yet
	int countRank = 0;
	//countRank makes sure that cards of one rank go towards count1 and another rank go towards count2
	for (int i = 0; i < 5; i++) {
		//forwards match in array
		if (i!=4 && deck->getCard(i)->getRank() == deck->getCard(i+1)->getRank()) {
			if (countRankSet && deck->getCard(i)->getRank() != countRank) {
				count2++;
			} else {
				countRankSet = true;
				countRank = deck->getCard(i)->getRank();
				count1++;
			}
		} 
		//backwards match in array
		else if (i!=0 && deck->getCard(i)->getRank() == deck->getCard(i-1)->getRank()) {
			if (countRankSet && deck->getCard(i)->getRank() != countRank) {
				count2++;
			} else {
				countRankSet = true;
				countRank = deck->getCard(i)->getRank();
				count1++;
			}
		}
	}
	//set count to the highest of count1 and count2
	if (count1 >= count2) count = count1;
	else count = count2;
	//Full House conditions
	if (count1 + count2 == 5 && count1 != 5) {
		compR(3);
	}
	//Two Pair conditions
	if (count1 == 2 && count2 == 2) {
		compR(7);
	}
	//Four of a Kind conditions
	if (count == 4) {
		compR(2);
	} 
	//Three of a Kind condtions
	else if (count == 3) {
		compR(6);
		return;
	} 
	//Pair condtions
	else if (count == 2) {
		compR(8);
	}
}

void Logic::checkFlushStraight() {
	if (sameSuit == true) {
		compR(4);
	}
	if (straight == true) {
		compR(5);
	}
}

void Logic::checkHigh() {
	//Goes through deck and finds the highest card
	int highest = -1;
	for (int i = 0; i < 5; i++) {
		if (deck->getCard(i)->getRank() == 0) {highest = 13;}
		if (deck->getCard(i)->getRank() == 1) {highest = 14;}
		if (deck->getCard(i)->getRank() > highest) {highest = deck->getCard(i)->getRank();}
	}
	if (highest == 13) {compR(10); return;}
	if (highest == 14) {compR(9); return;}
	compR((12-highest)+11);
}

void Logic::printResult () {
	cout << "You have ";
	//Prints out the best hand found
	switch(r) {
		case 0: cout << "a Royal Flush"; break;
		case 1: cout << "a Straight Flush"; break;
		case 2: cout << "a Four Of A Kind"; break;
		case 3: cout << "a Full House"; break;
		case 4: cout << "a Flush"; break;
		case 5: cout << "a Straight"; break;
		case 6: cout << "a Three Of A Kind"; break;
		case 7: cout << "a Two Pair"; break;
		case 8: cout << "a Pair"; break;
		case 9: cout << "an Ace High"; break;
		case 10: cout << "a King High"; break;
		case 11: cout << "a Queen High"; break;
		case 12: cout << "a Jack High"; break;
		case 13: cout << "a Ten High"; break;
		case 14: cout << "a Nine High"; break;
		case 15: cout << "a Eight High"; break;
		case 16: cout << "a Seven High"; break;
		default: cout << "Trash"; break;
	}
	cout << "." << endl;
}

void Logic::compR(int num) {
	//If the hand is better, lower r to represent that hand
	if (num < r) r = num;
}
