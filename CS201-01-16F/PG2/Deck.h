/*
 * Deck.h
 *
 *  Created on: Sep 12, 2016
 *      Author: Britney Reese
 */

#ifndef DECK_H_
#define DECK_H_

#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;
#include "Card.h"

class Deck {

private:
	//Array holding objects of type Card
	Card ** Cards;
	//Size of previous array
	int sz;

public:
	//Inits Cards, sets size
	Deck(int size);
	//Adds objects of type Card to Cards
	void addCards();
	//Add an individual card
	void addCard(int num, int val);
	//Shuffles the objects of type Card in Cards
	void shuffleDeck();
	//Organizes the objects of type Card in Cards
	void organizeDeck();
	//Gets Cards
	Card ** getDeck();
	//Prints name of the objects of type Card in Cards
	void printDeck();
	//Gets 1 Card
	Card * getCard (int cd);
	//Gets size
	int getSize ();
	//Kills Deck
	~Deck();
};

#endif /* DECK_H_ */
