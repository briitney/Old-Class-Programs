/*
 * Main.cpp
 *
 *  Created on: Sep 13, 2016
 *      Author: Britney Reese
 */

//Main method of the game

#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;
#include "Card.h"
#include "Deck.h"
#include "Main.h"
#include "Logic.h"

int main () {
	int size;
	cout << "How many cards are there? ";
	cin >> size;
	cin.ignore();
	//Creates a deck of a certain size
	Deck* initDeck = new Deck(size);
	//Adds cards to it
	initDeck->addCards();
	//Shuffles the deck
	initDeck->shuffleDeck();
	//Creates a 5 card deck (a hand)
	Deck* deck = new Deck(5);
	//Makes the first five cards of the 5 cards deck the first five cards of initDeck
	if (size >= 5) {
		for (int i = 0; i < 5; i++) {
			deck->addCard(i, initDeck->getCard(i)->getValue());
		}
	} else {
		cout << "That's not enough cards!" << endl << "Ending program..." << endl;
		return 0;
	}

	//prints the deck
	deck->printDeck();
	//does logic
	new Logic(deck);

	//dealocate objects
	delete deck;
	delete initDeck;
	return 0;
}
