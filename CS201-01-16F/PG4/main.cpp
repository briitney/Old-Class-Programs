/*
 * main.cpp
 *
 *  Created on: Oct 23, 2016
 *      Author: Britney Reese
 *       Class: CS 201 Fall 2016
 */

#include <iostream>
#include <string>
#include <cstdlib>
#include "main.h"
#include "LinkedList.h"
using namespace std;

int main (int argc, char** argv) {
	set = false;
	string command;
	//Will accept commands and run corresponding methods until exit is typed
	while (true) {
		cout << "Enter a command: ";
		getline(cin, command);
		//command = command.substr(0, command.length()-1);
		//This line is need for the g++ compiler
		cout << command << endl;
		if (toUpper(command.substr(0,3))=="ADD") {
			int argLen = command.length()-command.find("\"")-2;
			string arg = command.substr(command.find("\"")+1, argLen);
			add(arg);
		} else if (toUpper(command) == "PRINT") {
			print();
		} else if (toUpper(command.substr(0, 6))=="REMOVE") {
			int argLen = command.length()-command.find("\"")-2;
			string arg = command.substr(command.find("\"")+1, argLen);
			remove(arg);
		} else if (toUpper(command) == "EXIT") {
			cout << "Goodbye." << endl;
			break;
		} else {
			cout << "That was not a valid command." << endl;
		}
	}
	if (set == true) lib->deleteList();
	return 0;
}

void add(string title) {
	if (set == false) {
		lib = new LinkedList(title);
		set = true;
	} else {
		lib->Add(title);
	}
}

void print() {
	if (set == false) {
		cout << "The list is empty!" << endl;
	} else {
		lib->print();
	}
}

void remove(string title) {
	if (set == false) {
		cout << "There is nothing to remove!" << endl;
	} else {
		lib->Remove(title);
	}
}

string toUpper (string title) {
	//copy pasted from my LinkListNode class
	string upped = "";
	for (int i = 0; i < (int)(title.length()); i++) {
		if (title[i] >= 'a' && title[i] <= 'z') {
			upped += title[i]-32;
		} else {
			upped += title[i];
		}
	}
	return upped;
}
