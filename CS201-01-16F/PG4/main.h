/*
 * main.h
 *
 *  Created on: Oct 23, 2016
 *      Author: Britney Reese
 *       Class: CS 201 Fall 2016
 */

#ifndef MAIN_H_
#define MAIN_H_


#include <iostream>
#include <string>
#include <cstdlib>
#include "LinkedList.h"
using namespace std;

class LinkedList;
//bool set lets you know if you already created a copy of the linked list (or not)
bool set;
LinkedList* lib;
int main (int argc, char** argv);
void add(string title);
void print();
void remove (string title);
string toUpper (string title);

#endif /* MAIN_H_ */
