/*
 * LinkedListNode.cpp
 *
 *  Created on: Oct 23, 2016
 *      Author: Britney Reese
 *       Class: CS 201 Fall 2016
 */

#include <iostream>
#include <string>
#include <cstdlib>
#include "LinkedListNode.h"
using namespace std;

string LinkedListNode::toUpper (string title) {
	//copy pasted from PG3
	string upped = "";
	for (int i = 0; i < (int)(title.length()); i++) {
		if (title[i] >= 'a' && title[i] <= 'z') {
			upped += title[i]-32;
		} else {
			upped += title[i];
		}
	}
	return upped;
}

LinkedListNode::LinkedListNode (string title, LinkedListNode* nextNode) {
	bookTitle = title;
	//create an uppercase version of the book title for comparison
	bookTitleUpper = toUpper(bookTitle);
	next = nextNode;
}

void LinkedListNode::Add (LinkedListNode* &prev, string title) {
	//Adds using the previous node's next pointer
	if (toUpper(title) < bookTitleUpper) {
		prev = new LinkedListNode(title, this);
	//checks for duplicates
	} else if (toUpper(title) == bookTitleUpper) {
		cout << "You already have a copy of this book in your library." << endl;
		return;
	}else if (next == NULL) {
		next = new LinkedListNode(title, NULL);
	} else {
		next->Add(next, title);
	}
}

void LinkedListNode::print() {
	cout << bookTitle << endl;
	if (next != NULL) {
		next->print();
	} else cout << endl;
}

//In order to make sure all nodes are transversed, move through to the end of the list then move backwards
//Also, making sure to set next to NULL where appropriate instead of just deleting it, because then errors happen.
void LinkedListNode::Remove(LinkedListNode* &prev, string title) {
	if (next != NULL) next->Remove(next, title);
	//Making use of string::find(string arg);
	if (next == NULL && bookTitleUpper.find(toUpper(title))!=string::npos) {
		prev = NULL;
		delete this;
	}
	else if (next != NULL && bookTitleUpper.find(toUpper(title))!=string::npos && next ) {
		prev = next;
		delete this;
	}
}

void LinkedListNode::deleteStuff(LinkedListNode* prev) {
	//deletes all nodes after the current node
	if ( next!= NULL) {
		next->deleteStuff(this);
	}
	prev->next = NULL;
	delete this;
}
