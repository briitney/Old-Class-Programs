/*
 * LinkedList.h
 *
 *  Created on: Oct 23, 2016
 *      Author: Britney Reese
 *       Class: CS 201 Fall 2016
 */

#ifndef LINKEDLIST_H_
#define LINKEDLIST_H_

#include <iostream>
#include <string>
#include <cstdlib>
#include "LinkedListNode.h"
using namespace std;

class LinkedListNode;

class LinkedList {
private:
	LinkedListNode* head;

public:
	LinkedList(string title);
	void Add(string title);
	void print();
	void Remove(string title);
	//Used to delete the whole list at once
	void deleteList();
};

#endif /* LINKEDLIST_H_ */
