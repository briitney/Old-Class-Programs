/*
 * LinkedListNode.h
 *
 *  Created on: Oct 23, 2016
 *      Author: Britney Reese
 *       Class: CS 201 Fall 2016
 */

#ifndef LINKEDLISTNODE_H_
#define LINKEDLISTNODE_H_


#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;

class LinkedListNode {
private:
	string bookTitle;
	//Upper case version of the book title used for comparison
	string bookTitleUpper;
	LinkedListNode* next;

public:
	string toUpper(string title);
	LinkedListNode(string title, LinkedListNode* nextNode);
	void Add(LinkedListNode* &prev, string title);
	void print();
	void Remove(LinkedListNode* &prev, string title);
	void deleteStuff(LinkedListNode* prev);
};


#endif /* LINKEDLISTNODE_H_ */


