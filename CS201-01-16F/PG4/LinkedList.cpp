/*
 * LinkedList.cpp
 *
 *  Created on: Oct 23, 2016
 *      Author: Britney Reese
 *       Class: CS 201 Fall 2016
 */

//All of these methods just check if the head exists, and if it does, runs the LinkedListNode version of the method on the head.

#include <iostream>
#include <string>
#include <cstdlib>
#include "LinkedList.h"
#include "LinkedListNode.h"
using namespace std;

LinkedList::LinkedList (string title){
	head = new LinkedListNode(title, NULL);
}

void LinkedList::Add (string title) {
	if (head != NULL) {
		head->Add(head, title);
	} else head = new LinkedListNode(title, NULL);
}

void LinkedList::print() {
	if (head == NULL) {
		cout << "The list is empty!" << endl;
	} else {
		head->print();
	}
}

void LinkedList::Remove(string title) {
	if (head != NULL) head->Remove(head, title);
	else cout << "The list is empty!" << endl;
}

void LinkedList::deleteList() {
	//deletes all nodes and itself
	if (head != NULL) {
		head->deleteStuff(head);
	}
	delete this;
}
