/*
 * main.cpp
 *
 *  Created on: Sep 30, 2016
 *      Author: Britney Reese
 *       Class: CS 201 Fall 2016
 */
//This program searches a text file containing one line of words which are the same length.
//The first four characters denote the length of the words.
//This program ignores case

#include "main.h"
#include <cmath>
using namespace std;

//I don't want to pass these variables around
int wordSize;
int wordNum;

int main (int argc, char** argv) {
	cout << "What is the name of the text file you would like to search? ";
	string input;
	//getline was including the new line, so I used cin instead
	cin >> input;
	fstream f (input.c_str(), ios::in|ios::out|ios::binary|ios::app);
	wordSize = findWordSize(f);
	wordNum = findWordNum(f);
	cout << "What record are you searching for? ";
	string record;
	cin >> record;
	bool found = binarySearch(f, record, 0, wordNum-1, (wordNum-1)/2);
	if (found == true) {
		cout << "The record is present." << endl;
	} else {
		cout << "The record is not present." << endl;
	}
	return 0;
}

//Gets a word at a location from the file
string readWord (fstream &f, int start) {
	string a = "";
	for (int i = 0; i < wordSize; i++) {
		f.seekg(start+i,ios::beg);
		char ch;
		f.get(ch);
		a += ch;
	}
	return a;
}

//Makes a string uppercase for comparison reasons
string toUpper (string upIt) {
	string upped = "";
	for (int i = 0; i < (int)(upIt.length()); i++) {
		if (upIt[i] >= 'a' && upIt[i] <= 'z') {
			upped += upIt[i]-32;
		} else {
			upped += upIt[i];
		}
	}
	return upped;
}

//Turns the first four bytes into a number to find the word size
int findWordSize (fstream &f) {
	int a = 0;
	for (int i = 0; i < 4; i++) {
		f.seekg(i,ios::beg);
		char ch;
		f.get(ch);
		a += ((int)pow(10.0,3.0-i))*((int)ch-48);
	}
	return a;
}

//Finds the number of words in the file
int findWordNum (fstream &f) {
	f.seekg(0,ios::end);
	return ((int)f.tellg()-4)/wordSize;
}

//Recursively binary searches the file
bool binarySearch (fstream &f, string record, int low, int high, int mid) {
	if (low > high) {return false;}
	string Mid = readWord(f, 4+mid*wordSize);
	//Compares strings as uppercase
	string upMid = toUpper(Mid);
	string upRecord = toUpper(record);
	if (upMid == upRecord) {return true;}
	else if (upMid > upRecord) {return binarySearch(f, record, low, mid-1, (mid-1+low)/2);}
	else if (upMid < upRecord) {return binarySearch(f, record, mid+1, high, (high+mid+1)/2);}
}
