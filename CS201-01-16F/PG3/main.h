/*
 * main.h
 *
 *  Created on: Sep 30, 2016
 *      Author: Britney Reese
 *       Class: CS 201 Fall 2016
 */
//Header for main.cpp, boring normal header stuff.

#ifndef MAIN_H_
#define MAIN_H_

#include <iostream>
#include <string>
#include <cstdlib>
#include <fstream>
using namespace std;

int main (int argc, char** argv);
string readWord (fstream &f, int start);
string toUpper (string upIt);
int findWordSize (fstream &f);
int findWordNum (fstream &f);
bool binarySearch (fstream &f, string record, int low, int high, int mid);

#endif /* MAIN_H_ */
