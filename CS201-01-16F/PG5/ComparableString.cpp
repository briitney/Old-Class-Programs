//Britney Reese, CS 201 01 16f
//This is a Comparable that holds a string.

#include "ComparableString.h"
#include "ComparableInt.h"
#include <iostream>
using namespace std;

ComparableString::ComparableString (string S) {
	s = S;
}

int ComparableString::cmp (Comparable *c) {
	ComparableString *A = (ComparableString *)c;
	string myString = s;
	string compString = A->gets();
	//Get the uppercase version of my string and the string
	//being compared for comparison.
	for (int i = 0; i < myString.length(); i++) {
		if (myString[i] >= 'a' && myString[i] <= 'z') {
			myString[i] = (char)((int)myString[i]-32);
		}
	}
	for (int i = 0; i < compString.length(); i++) {
		if (compString[i] >= 'a' && compString[i] <= 'z') {
			compString[i] = (char)((int)compString[i]-32);
		}
	}
	//Simple string comparison.
	if (myString == compString) {
		return 0;
	} else if (myString > compString) {
		return 1;
	} else {
		return -1;
	}
}

void ComparableString::print () {
	cout << s;
}

string ComparableString::gets() {
	return s;
}
