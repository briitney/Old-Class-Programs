//Britney Reese, CS 201 01 16f
//This is a Comparable that holds an int.

#include "ComparableInt.h"
#include "Comparable.h"
#include <iostream>
using namespace std;

ComparableInt::ComparableInt (int I) {
	i = I;
}

int ComparableInt::cmp (Comparable *c) {
	//Simple integer comparison.
	ComparableInt *A = (ComparableInt *)c;
	if (i == A->geti()) {
		return 0;
	} else if (i > A->geti()) {
		return 1;
	} else {
		return -1;
	}
}

void ComparableInt::print () {
	cout << i;
}

int ComparableInt::geti() {
	return i;
}
