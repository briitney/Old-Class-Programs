//Britney Reese, CS 201 01 16f
//Main method of the program, asks the user for two arrays,
//one of int and the other of string, and gives the largest and
//smallest of the respective arrays.

#include <iostream>
#include "PG5.h"
#include "ComparableArray.h"
#include "ComparableInt.h"
#include "ComparableString.h"

using namespace std;

int main (int argc, char **argv) {
	string a;
	cout << "How many integers? ";
	int intArrLen;
	cin >> intArrLen;
	getline(cin, a);
	cout << intArrLen << endl;
	//Create a new ComparableArray and add entries to it.
	ComparableArray* IA = new ComparableArray(intArrLen);
	for (int i = 0; i < intArrLen; i++) {
		int nw;
		cout << "Enter an integer: ";
		cin >> nw;
		getline(cin, a);
		cout << nw << endl;
		IA->seti(i, new ComparableInt(nw));
	}
	//Print the largest and smallest values in that array.
	cout << endl << "The largest integer is ";
	IA->largest()->print();
	cout << " and the smallest is ";
	IA->smallest()->print();
	cout << "." << endl << endl;

	//Repeat the above code except using ComparableString to
	//show the versatility of objects.
	cout << "How many strings? ";
	int stringArrLen;
	cin >> stringArrLen;
	getline(cin, a);
	cout << stringArrLen << endl;
	ComparableArray* SA = new ComparableArray(stringArrLen);
	for (int i = 0; i < stringArrLen; i++) {
		string nw;
		cout << "Enter a string: ";
		getline(cin, nw);
		cout << nw << endl;
		SA->seti(i, new ComparableString(nw));
	}
	cout << endl << "The largest string is ";
	SA->largest()->print();
	cout << " and the smallest is ";
	SA->smallest()->print();
	cout << "." << endl << endl;
}
