//Britney Reese, CS 201 01 16f
//This is the class for an array of type Comparable; it can hold both
//ComparableInts and ComparableStinrgs.
#include "Comparable.h"
#include "ComparableArray.h"
#include "ComparableInt.h"

ComparableArray::ComparableArray(int s) {
	sz = s;
	CA = new Comparable * [sz];
}

Comparable * ComparableArray::smallest () {
	Comparable * A = CA[0];
	//Loops through the array and finds the smallest entry.
	for (int i = 0; i < sz; i++) {
		if (CA[i]->cmp(A) < 0) {
			A = CA[i];
		}
	}
	return A;
}

Comparable * ComparableArray::largest () {
	Comparable * A = CA[0];
	//Loops through the array and finds the largest entry.
	for (int i = 0; i < sz; i++) {
		if (CA[i]->cmp(A) > 0) {
			A = CA[i];
		}
	}
	return A;
}

void ComparableArray::seti (int i, Comparable *c) {
	CA[i] = c;
}

ComparableArray::~ComparableArray () {
	for (int i = 0; i < sz; i++) {
		delete CA[i];
	}
}
