//Britney Reese, CS 122 03 16w
//This is a secret code generator.

import java.util.*;
import java.io.*;

public class SecretCode {
	private static Scanner input;
	public static void main (String[] args) {
		//This is the main method that takes input and prints output
		//Get my input
		System.out.println("Enter a string: ");
		input = new Scanner(System.in);
		String textstuff = input.nextLine();
		//Use Strange on the String and print the result
		System.out.println(Strange(textstuff));
	}

	public static String Strange ( String e ) {
		//This is where the bulk of the work is done,
		//including the actual 'coding' of the message
		String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String alphabetlower = "abcdefghijklmnopqrstuvwxyz";
		String plus = "";
		//Loop through the whole string
		for (int i = 0; i < e.length(); i++) {
			if (e.charAt(i) >= 'A' && e.charAt(i) <= 'Z') {
				//Checks each letter in the alphabet
				for (int j = 0; j < alphabet.length(); j++) {
					//Switches first half
					if (e.charAt(i) == alphabet.charAt(j) && j < 13) {
						plus = plus + alphabet.charAt(12 - j);
					}
					//Switches second half
					else if ( e.charAt(i) == alphabet.charAt(j) && j >= 13) {
						plus = plus + alphabet.charAt(13+25-j);
					}
				}
			}
			//Lowercase code is pretty much copy paste of uppercase code with variables changed
			else if (e.charAt(i) > 'a' && e.charAt(i) <= 'z') {
				for (int j = 0; j < alphabetlower.length(); j++) {
					if (e.charAt(i) == alphabetlower.charAt(j) && j < 13) {
							plus = plus + alphabetlower.charAt(12 - j);
					} else if ( e.charAt(i) == alphabetlower.charAt(j) && j >= 13) {
						plus = plus + alphabetlower.charAt(13+25-j);
					}
				}
			}
			//All other characters
			else plus = plus + e.charAt(i);
		}
		return plus;
	}
}