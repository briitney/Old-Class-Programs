//CS 122 W16 Britney Reese
//This is a class that creates a student, that holds both double grade and a string name
import java.io.*;
import java.util.*;

public class Student {
	private double grade;
	private String name;
	//Create
	public Student (String Name, double Grade) {
		grade = Grade;
		name = Name;
	}
	//Getter
	public String getName () { return name; }
	public double getGrade () { return grade; }
}