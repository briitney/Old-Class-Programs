//CS 122 W16 Britney Reese
//This is a class that creates a student, that holds both double grade and a string name
import java.io.*;
import java.util.*;

public class DB {
	private static Scanner in;
	//Array of students
	private static Student[] Kids;
	//Main method
	public static void main (String[] args) {
		in = new Scanner(System.in);
		String Command = "";
		Kids = new Student[0];
		while (true) {
			System.out.println("Enter Command: ");
			Command = in.nextLine();
			//Runs a method depending on what the user enters
			if (Command.compareToIgnoreCase("LIST") == 0) {
				LIST(Kids);
			} else if (Command.compareToIgnoreCase("ADD") == 0) {
				ADD();
			} else if (Command.compareToIgnoreCase("DELETE") == 0) {
				DELETE();
			} else if (Command.compareToIgnoreCase("SEARCH") == 0) {
				implementSEARCH();
			} else if (Command.compareToIgnoreCase("QUIT")== 0) {
				break;
			} else {
				System.out.println("I'm sorry. I do not understand: "+Command+". Please try again.");
			}
		}

	}
	//Lists all the enties in the array
	public static void LIST (Student[] Anything) {
		for (int i = 0; i < Anything.length; i++){
			System.out.println(Anything[i].getName()+" has a grade of "+Anything[i].getGrade()+".");
		}
	}

	//Adds an entry to the array
	public static void ADD () {
		System.out.println("What student do you wish to add? ");
		String name = in.nextLine();
		System.out.println("What is this student's course average? ");
		double grade = Double.parseDouble(in.nextLine());
		Student addingStudent = new Student(name, grade);
		if (Kids.length == 0) {
			Kids = new Student[1];
			Kids[0] = addingStudent;
			return;
		}
		//Gets location the student would be
		double studentLocation = SEARCH(0, Kids.length-1, (Kids.length-1)/2, addingStudent.getName());
		if (addingStudent.getName().compareTo(Kids[(int)studentLocation].getName()) == 0) {
			System.out.println("That student already exists.");
		} else {
			//Adds to array in that location
			Student[] KidsNew = new Student[Kids.length+1];
			if (studentLocation < 0) KidsNew[(int)studentLocation] = addingStudent;
			else KidsNew[(int)studentLocation+1] = addingStudent;
			for (int i = 0; i < Kids.length; i++) {
				if ( i < studentLocation ) {
					KidsNew[i] = Kids[i];
				} else {
					KidsNew[i+1] = Kids[i];
				}
			}
			Kids = KidsNew.clone();
			System.out.println("This student has been added.");
		}
	}

	//Deletes a student from the array
	public static void DELETE () {
			System.out.println("What student do you wish to delete? ");
			String name = in.nextLine();
			//Finds the student's location
			double studentLocation = SEARCH(0, Kids.length-1, (Kids.length-1)/2, name);
			if (studentLocation % 1 != 0 ) {
				System.out.println("That student doesn't exist.");
			} else {
				//Recreates array without that student
				Student[] KidsNew = new Student[Kids.length-1];
				for (int i = 0; i < Kids.length-1; i++) {
					if ( i < studentLocation ) {
						KidsNew[i] = Kids[i];
					} else {
						KidsNew[i] = Kids[i+1];
					}
				}
				Kids = KidsNew.clone();
				System.out.println("This student has been deleted.");
			}
	}

	//Searches the arrray, then if found, returns a whole number, else
	//returns a number ending in .5 between the index of the entries it
	//falls between
	public static double SEARCH (int start, int end, int middle, String name) {
		if ( end < 0 ) { return -0.5; }
		if ( start >= Kids.length) { return Kids.length-.5; }
		if (start > end ) {return (double)middle+0.5;}
		if (Kids[middle].getName().compareTo(name) > 0) {
			if (end != middle) end = middle;
			else end--;
			middle = (start+end)/2;
			return SEARCH(start, end, middle, name);
		} else if (Kids[middle].getName().compareTo(name) < 0) {
			if (start != middle) start = middle;
			else start++;
			middle = (start+end)/2;
			return SEARCH(start, end, middle, name);
		} else {
			return (double)middle;
		}
	}

	//Runs the binary search
	public static void implementSEARCH() {
		System.out.println("What student are you looking for?");
		String StudentName = in.nextLine();
		double position = SEARCH(0, Kids.length-1, (Kids.length-1)/2, StudentName);
		if (Kids.length == 0) {System.out.println("The database is empty");}
		else if (position == -0.5) {System.out.println(StudentName+" is not in the database but would fall before "+Kids[0].getName());}
		else if (position == Kids.length-0.5) {System.out.println(StudentName+" is not in the database but would fall after "+Kids[Kids.length-1].getName());}
		else if (position % 1 != 0) {System.out.println(StudentName+" is not in the database but would fall after "+Kids[(int)(position-0.5)].getName()+" and before "+Kids[(int)(position+0.5)].getName()+".");}
		else System.out.println(Kids[(int)position].getName()+" has a couse average of "+Kids[(int)position].getGrade()+".");
	}

}