//Britney Reese, CS 122 Winter 2016
//This program creates an expression tree using postfix notation, and evaluates it.
import java.io.*;
import java.util.*;

public class ReversePolish {
	private static Scanner in;
	private static Input item;
	//Array to store trees and arrCurr to keep track
	//of where I am in the array
	private static TreeNode[] arrTree;
	private static int arrCurr;

	public static void main (String agrs[]) {
		Scanner in = new Scanner( System.in );
		println("Enter a postfix expression: ");
		String expression = in.nextLine();
		item = new Input(expression);
		//Leaves two open spots in the array so I don't get
		//a null pointer exception when trying to access
		//the two positions before my main tree.
		arrTree = new TreeNode[item.getLength()+3];
		arrCurr = 2;
		CreateTree();
		PrintThings();
		EvaluateThings();
	}

	public static void println ( String s ) {
		System.out.println(s);
	}
	public static void print ( String s ) {
		System.out.print(s);
	}
	public static Scanner in () {
		return in;
	}
	//Adds items into the array of trees and stitches other trees together
	public static void CreateTree () {
		if(item.isEnd()) return;
		String current = item.getNext();
		if ( current.compareTo("+") == 0 || current.compareTo("-") == 0 || current.compareTo("/") == 0 || current.compareTo("*") == 0 ) {
			TreeNode a = new TreeNode(current, arrTree[arrCurr-2], arrTree[arrCurr-1]);
			arrCurr = arrCurr-2;
			arrTree[arrCurr] = a;
			arrCurr++;
		} else {
			arrTree[arrCurr] = new TreeNode(current, null, null);
			arrCurr++;
		}
		CreateTree();
	}
	public static void PrintThings () {
		if (arrTree[2] == null) {println("The tree is empty.");return;}
		arrTree[2].PreFix();
		println("");
		arrTree[2].InFix();
		println("");
	}
	public static void EvaluateThings () {
		if (arrTree[2] == null) {println("The tree is empty.");return;}
		println(""+arrTree[2].Eval());
	}
}

class TreeNode {
	private String info;
	private TreeNode left;
	private TreeNode right;
	public TreeNode (String s, TreeNode l, TreeNode r) {
		info = s;
		left = l;
		right = r;
	}

	public void PostFix () {
		if (left != null) left.PostFix();
		if (right != null) right.PostFix();
  		ReversePolish.print(info+" ");
	}
	public void PreFix () {
		ReversePolish.print(info+" ");
		if (left != null) left.PreFix();
 		if (right != null) right.PreFix();
	}
	public void InFix () {
		if (left != null) {ReversePolish.print("( ");left.InFix();}
		ReversePolish.print(info+" ");
		if (right != null) {right.InFix();ReversePolish.print(") ");}
	}
	//Evaluates value of the expression stored in tree
	public int Eval () {
		if (info.compareTo("+") == 0 ) {
			return left.Eval()+right.Eval();
		} else if (info.compareTo("-") == 0 ) {
			return left.Eval()-right.Eval();
		} else if (info.compareTo("/") == 0 ) {
			return left.Eval()/right.Eval();
		} else if (info.compareTo("*") == 0 ) {
			return left.Eval()*right.Eval();
		} else {
			return Integer.parseInt(info);
		}
	}
}

//My own class that goes through the input string,
//keeping track of where I am in the string
class Input {
	private String input;
	private int progress;
	private int length;

	public Input (String s) {
		input = s;
		progress = 0;
		length = s.length();
	}

	public int getProg () {return progress;}

	public void setProg(int a) {progress = a;}

	public int getLength () {return length;}

	public String getNext () {
		if (progress<length) {
			progress++;
			return ""+input.charAt(progress-1);
		} else return "Error";
	}

	public boolean isEnd () {
		if (progress<length) return false;
		return true;
	}
}