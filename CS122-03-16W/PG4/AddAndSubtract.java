//Britney Reese, CS 122 03 16w
//This program computes the value of a sequence
//f(n-1) + f(n-2) - (f(n-3)+f(n-4)) at a given n
import java.io.*;
import java.util.*;

public class AddAndSubtract {
	private static Scanner in;
	//Asks for the input and outputs the answer
	public static void main (String[] args) {
		in = new Scanner (System.in);
		System.out.println("What argument will you use?");
		int m = in.nextInt();
		System.out.print("The Value of the function is: ");
		System.out.println(Main(m));
	}

	//Calculates the result
	public static int Main (int n) {
		//Base case
		if (n < 4) return n;
		//Recursive case
		return Main(n-1) + Main(n-2) - (Main(n-3)+Main(n-4));
	}
}