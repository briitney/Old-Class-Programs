//Britney Reese, CS 122 03 16w
//This program computes the value of a number in Pascal's Pyramid.
import java.io.*;
import java.util.*;

public class PascalPyramid {
	private static Scanner in;
	public static void main (String[] args) {
		//Asks for input and outputs the answer
		in = new Scanner (System.in);
		System.out.println("Enter the triangle");
		int triangle = in.nextInt();
		System.out.println("Enter the row");
		int row = in.nextInt();
		System.out.println("Enter the column");
		int column = in.nextInt();
		System.out.print("The number is: ");
		System.out.println(Pascal (triangle, row, column));
	}

	public static int Pascal (int triangle, int row, int column) {
		//Cases for the sides of a given triangle, including the
		//three vertexes equaling 1 as base cases.
		if (column == 0) {
			if (row == 0) return 1;
			if (row == triangle) return 1;
			return Pascal(triangle - 1, row, column) + Pascal(triangle - 1, row - 1, column);
		}
		if (row == triangle) {
			if (row == column) return 1;
			return Pascal(triangle - 1, row - 1, column) + Pascal(triangle - 1, row - 1, column - 1);
		}
		if (row == column) {
			return Pascal(triangle - 1, row, column) + Pascal(triangle - 1, row - 1, column - 1);
		}
		//The regular case for an entry in the triangle
		return Pascal(triangle - 1, row, column) + Pascal(triangle - 1, row -1, column) + Pascal(triangle - 1, row -1, column - 1);
	}
}