//Britney Reese, CS 120
//This is a program that compares the speeds of the insertion sort and the shaker sort
import java.io.*;
import java.util.*;

public class SortsDebug {
	private static Scanner in = new Scanner(System.in);
	public static void main (String[] args) {
		//Accepts information on how many items will be entered
		System.out.println("How many Strings? ");
		int arraySize = Integer.parseInt(in.nextLine());
		String[] initialArray = new String[arraySize];
		//Accepts items into array
		if ( arraySize == 1 ) {
			System.out.println("Enter a String: ");
			String t = in.nextLine();
			for (int i = 1; i < t.length()-1; i++) {
				if (t.charAt(i) == ' ') {
					arraySize++;
				}
			}
			initialArray = new String[arraySize];
			for (int i = 0; i <initialArray.length; i++) {
				initialArray[i] = "";
			}
			int b = 0;
			for (int i = 0; i <t.length(); i++) {
				if (t.charAt(i) == ' ') {
					b++;
				} else {initialArray[b] += t.charAt(i);}
			}
		}
		else {
			int enterCount = 1;
			for (int i=0; i < arraySize; i++) {
				System.out.println("Enter String "+enterCount+": ");
				initialArray[i] = in.nextLine();
				enterCount++;
			}
		}
		//Duplicates array, so both sorts can be run against
		//each other
		String[] initialArray2 = new String[arraySize];
		for (int i = 0; i < arraySize; i++) {
			initialArray2[i] = initialArray[i];
		}
		//Run and store information
		System.out.println("Insertion sorting... ");
		int ins = InsertionSort(initialArray);
		System.out.println("Shaker sorting... ");
		int shk = ShakerSort(initialArray2);
		//Prints results
		System.out.println("Insertion Sort used "+ins+" comparisons.");
		System.out.println("Shakersort used "+shk+" comparisons.");
		if (shk > ins) {System.out.println("Insertion Sort was fastest");}
		else if (ins > shk) {System.out.println("Shaker Sort was fastest");}
		else {System.out.println("It was a tie!");}

	}

	//Most of code provided by the great and wise Andy Poe,
	//Although modified to use PrintArray() and
	//keep a counter of comparisons
	public static int InsertionSort (String[] B) {
		int sz = B.length;
		int count = 0;
		for (int i = 0; i < sz-1; i++) {
			for (int j = i; j >= 0; j--) {
				count++;
				if (!(B[j].compareTo (B[j+1]) > 0)) {break;}
				String t = B[j];  //switch A[j], A[j+1]
				B[j] = B[j+1];
				B[j+1] = t;
			}
		}
		PrintArray(B);
		System.out.println();
		return count;
	}

	//Shakersort, running from left to right, then right to left, etc.
	public static int ShakerSort (String[] A) {
		int sz = A.length;
		int count = 0; //count is number of comparisons
		int start = 0; //start is the starting point of the loop
		int inarow = 0; //nmuber of 'good' comparisons in a row before
						//loop terminates
		int end = 0; //end is the number of posistions away from the
					 //ending point of the loop.
		boolean even = false; //if true, the loop is running left to right;
							  //if false, the loop is running right to left.
		for (int i = 0;start+end < sz-1; i++) {
			if (even == true) {
				for (int j = start; j < (sz-1)-end; j++) {
					count++;
					if (A[j].compareTo (A[j+1]) > 0){
						String t = A[j];  //switch A[j], A[j+1]
						A[j] = A[j+1];
						A[j+1] = t;
						inarow = 0;
						System.out.print(inarow+" ");
					} else {inarow++; System.out.print(inarow+" ");}
					PrintArray(A);
					System.out.println("Comparing "+A[j]+" and "+A[j+1]);
				}
			end = end + 1 + inarow;
			inarow = 0;
			}
			if (even == false) {
				for (int j = end; j < (sz-1)-start; j++) {
					count++;
					if (A[((sz-1)-j)-1].compareTo (A[(sz-1)-j]) > 0){
						String t = A[((sz-1)-j)-1];
						A[((sz-1)-j)-1] = A[(sz-1)-j];
						A[(sz-1)-j] = t;
						inarow = 0;
						System.out.print(inarow+" ");
					} else {inarow++; System.out.print(inarow+" ");}
					PrintArray(A);
					System.out.println(" Comparing "+A[((sz-1)-j)-1]+" and "+A[(sz-1)-j]);
				}
			start = start + 1 + inarow;
			inarow = 0;								// a b j d w l m a o
			}
			if (even == true) even = false;
			else if (even == false) even = true;
			System.out.println("Comparisons: "+count+" Start: "+A[start]+" ("+start+") End: "+A[(sz-1)-end]+" ("+end+")");
			System.out.println();
		}
		PrintArray(A);
		return count;
	}

	//This prints arrays, the code of which was also graciously provided by Andy.
	public static void PrintArray (String[] A) {
		for (int i=0; i < A.length; i++){
			System.out.print (A[i]+" ");
		}
	}

}