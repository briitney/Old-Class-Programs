//Britney Reese, CS 122 Winter 2016
//Auxiliary class for the DB class.
import java.io.*;
import java.util.*;

public class LinkedListNode {

	private double grade;
	private String name;
	private LinkedListNode next;
	private LinkedListNode previous;
	private DB main;

	//Various gets and sets.
	public String getName () {return name;}
	public double getGrade () {return grade;}
	public LinkedListNode getNext () {return next;}
	public LinkedListNode getPrevious () {return previous;}

	public void setName (String n) {name = n;}
	public void setGrade (double num) {grade = num;}
	public void setNext (LinkedListNode nxt) {next = nxt;}
	public void setPrevious (LinkedListNode prv) {previous = prv;}

	//Instantiation
	public LinkedListNode (LinkedListNode prv, String n, double num, LinkedListNode nxt) {
		previous = prv;
		grade = num;
		name = n;
		next = nxt;
	}

	//Prints all nodes in list.
	public void printAll () {
		main.print(name);
		if (next!=null) next.printAll();
	}

	//Adds a node in alphabetical order
	public void add (String n, double num) {
		if (n.compareToIgnoreCase(name) < 0) {
			previous.setNext(new LinkedListNode (previous, n, num, this));
			previous = previous.getNext();
		} else if (next == null) {
			next = new LinkedListNode(this, n, num, null);
		} else next.add(n, num);
	}

	//Deletes all instances of a node
	public void delete (String n) {
		if (name.compareTo(n) == 0) {
			previous.setNext(next);
			if (next != null) {
				next.setPrevious(previous);
			}
		}
		if (next != null) next.delete(n);
	}

	//Searches for the first node that the word comes after.
	public LinkedListNode search (String n) {
		if (name.compareToIgnoreCase(n) > 0)
			return previous;
		if (name.compareToIgnoreCase(n) == 0)
			return this;
		if (next != null) {
			return next.search(n);
		} else return this;
	}
}