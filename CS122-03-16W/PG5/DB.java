//Britney Reese, CS 122 Winter 2016
//This is a recreation of an earlier database program using linked list instead of arrays.

import java.io.*;
import java.util.*;

public class DB {
	private static Scanner in;
	//Head is the first entry in the linked list.
	private static LinkedListNode head;

	public static void main (String[] args) {
		in = new Scanner (System.in);
		head = null;
		boolean cont = true;
		//Loops over the program until quit is entered.
		while (cont == true) {
			print("Enter command: ");
			String command = in.nextLine();
			cont = commandDecision(command);
		}
	}

	//Carpal tunnel prevention
	//And also can let other classes access the scanner.
	public static void print (String n) {
		System.out.println(n);
	}

	//Collects the proper input and redirects to the proper method
	//Depending on what command was entered.
	public static boolean commandDecision (String command) {
		if (command.compareToIgnoreCase("List") == 0) {
			print("Listing...");
			list();
		} else if (command.compareToIgnoreCase("Add") == 0) {
			print("What student do you wish to add?");
			String n = in.nextLine();
			print("What is this student's course average?");
			double num = Double.parseDouble(in.nextLine());
			add(n,num);
			print("The student has been added.");
		} else if (command.compareToIgnoreCase("Delete") == 0) {
			print("What student do you wish to delete?");
			String n = in.nextLine();
			delete(n);
		} else if (command.compareToIgnoreCase("Search") == 0) {
			print("What student are you looking for?");
			String n = in.nextLine();
			search(n);
		} else if (command.compareToIgnoreCase("Quit") == 0) {
			print("Goodbye.");
			return false;
		} else {
			print("I'm sorry, I do not know that command.");
		}
		return true;
	}

	//Debugging method
	public static void list () {
		if (head != null) {head.printAll();}
	}

	//Adds a node to the list, first checking the head then the
	//rest of the list.
	public static void add ( String n, double num ) {
		if (head == null) head = new LinkedListNode(null, n, num, null);
		else if (n.compareToIgnoreCase(head.getName()) < 0) {
			head.setPrevious(new LinkedListNode(null, n, num, head));
			head = head.getPrevious();
		}
		else head.add(n, num);
	}

	//Deletes all matching nodes, including the head.
	public static void delete ( String n ) {
		if (head == null) print("There is no one to delete.");
		else {
			if (head.getNext() != null) {
				head.getNext().delete(n);
			}
			if (head.getName().compareTo(n) == 0) {
				head = head.getNext();
			}
		}
	}

	//Searches for the first instance of a node that matches.
	public static void search ( String n ) {
		if ( head == null ) print("There are no students in the database.");
		else {
			if (head.getName().compareToIgnoreCase(n) > 0)
				print(n+" would come before "+head.getName()+".");
			else {
				LinkedListNode a = head.search(n);
				if (a.getName().compareToIgnoreCase(n) == 0)
					print(n+"'s grade is "+head.getGrade()+".");
				else if (a.getNext() != null)
					print(n+" would come after "+a.getName()+" and before "+a.getNext().getName()+".");
				else print (n+" would come after "+a.getName()+".");
			}
		}
	}
}