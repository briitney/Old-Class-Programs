%{
#include "Parser.h"

int yyerror (const char * er);
extern int yylex ();
extern int yylineno;
extern char * yytext;
int string_count = 0;
int float_count = 0;

struct variable {
	char* name;
	char type;
	int offset;
};

// I know I'm wasting space by having both a float and a char* when I need one or the other
// The type and number are combined to make the label for the data
struct data {
	float f_val;
	char* s_val;
	char type;
	int index;
};

// Anything being stored as a varriable needs to know its name, what type of varriable it is, and its position on the stack.
forward_list<variable> myvars;

// Anything being stored as data just needs to know its type and value
forward_list<data> mydata;

%}

%union {
  int an_int;
  float a_float;
  char *a_string;
}

%token <an_int> INT
%token <a_float> FLOAT
%token <a_string> STRING
%token <a_string> WORD
%token COMMENCEMENT
%token ECRIVEZ

%%

PROGRAM : 
	{
		// Start the main method of the program
		cout << "\t.text\n\t.globl main\n";
	}
	FUNCTION_DECLS
	{
		cout << "\tli $v0, 10\n\tsyscall\n";
		print_data();
	}

FUNCTION_DECLS : FUNCTION_DECLS FUNCTION_DECL
	|
	;

FUNCTION_DECL : VALUE WORD '(' ')' '{' STATEMENTS '}'
	| VALUE COMMENCEMENT 
		{
			cout << "main:\n";
			// Initialize the frame pointer to the stack pointer
			cout << "\tmove $fp, $sp\n";
		}
		'(' ')' '{' STATEMENTS '}' 
	;

STATEMENTS : STATEMENTS STATEMENT
	|
	;
	
STATEMENT: FUNCTION_CALL ';'
	| error ';'
	;

FUNCTION_CALL : ECRIVEZ '(' PRINT_TYPE ')'
	| WORD '(' VALUE ')'
	;

PRINT_TYPE : 
	| INT 
		{
			cout << "\tli $v0, 1\n";
			cout << "\tla $a0, " << $1 << "\n";
			cout << "\tsyscall\n";
		}
	| FLOAT  
		{
			data d;
			d.type = 'f';
			d.f_val = $1;
			d.index = ++float_count;
			mydata.push_front(d);
			cout << "\tli $v0, 2\n";
			cout << "\tl.s $f12, fl" << float_count << "\n";
			cout << "\tsyscall\n";
		}
	| STRING  
		{
			data d;
			d.type = 's';
			d.s_val = $1;
			d.index = ++string_count;
			mydata.push_front(d);
			cout << "\tli $v0, 4\n";
			cout << "\tla $a0, str" << string_count << "\n";
			cout << "\tsyscall\n";
		}
	;

VALUE : INT
	| FLOAT
	| STRING
	| WORD
	;

%%

int yyerror(const char *msg) {
	fprintf(stderr, "line %d: %s at '%s'\n", yylineno, msg, yytext);
}

void print_data() {
	forward_list<data>::iterator my_iterator = mydata.begin();
	cout << "\t.data\n";
	for (auto it = mydata.begin(); it != mydata.end(); ++it) {
		if ((*it).type == 'f') {
			cout << "fl" << (*it).index << ":\t.float " << (*it).f_val << "\n";
		} else if ((*it).type == 's') {
			cout << "str" << (*it).index << ":\t.asciiz " << (*it).s_val << "\n";
		}
	}
}
