#ifndef SIMPLELEXER_H
#define SIMPLELEXER_H

#include <iostream>
#include <cstdlib>
#include <string>
#include <sstream>
using namespace std;

int countchar (string s, char a);
string stringify (int a);

#endif
