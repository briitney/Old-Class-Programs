%{

#include "SimpleLexer.h"

int linenumber = 1;

%}

int (\-|\+)?[0-9]+
float (\-|\+)?[0-9]+(\.[0-9]+)?((e|E)(\-|\+)?[0-9]+)?
variable [A-Za-z]([A-Za-z]|[0-9])*
string \"([^\n\"])*\"
beginning_comment \/\/.*
wrap_comment \/\*((\*(\**|([^\*/][^\*]*\*))*)|([^\*]([^\*]*|(\*+[^\*/]))*\**))\*\/
delimiter [\ \t\r\n]

%%

(\r\n|\n\r|\r|\n) {linenumber++;}

{int}/{delimiter}	{cout << "line "+stringify(linenumber)+": integer "+yytext << endl;}
{float}/{delimiter}	{cout << "line "+stringify(linenumber)+": float "+yytext << endl;}
{variable}/{delimiter}	{cout << "line "+stringify(linenumber)+": variable "+yytext << endl;}
{string}/{delimiter}	{cout << "line "+stringify(linenumber)+": string "+yytext << endl;}
{wrap_comment}|{beginning_comment}/{delimiter}	{linenumber+=countchar(yytext, '\n');}

[\ \t]	{}

[^\ \t\r\n]*/{delimiter} {cout << "line "+stringify(linenumber)+": Error \"" << yytext << "\"" << endl;}

%%

int yywrap () {
	return 1;
}

int main () {
	yylex();
}

int countchar (string s, char a) {
	int count = 0;
	for (int i = 0; i < s.size(); i++)
		if (s[i] == a) count++;
	return count;
}

string stringify (int a) {
	stringstream ss;
	ss << a;
	string str = ss.str();
	return str;
}
