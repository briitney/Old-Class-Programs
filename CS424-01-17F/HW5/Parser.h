#include <iostream>
#include <string>
#include <string.h>
#include <forward_list>
#include <algorithm>

#include "types.h"

#include <stdlib.h>

#include <strings.h>

using namespace std;

struct variable;

void print_data();

void makevar(const char* type, forward_list<char*>*);

variable* search_for_var(char* searchterm);

void intstuff (int myint, variable* myvar); 

void floatstuff (float myfloat, variable* myvar);
