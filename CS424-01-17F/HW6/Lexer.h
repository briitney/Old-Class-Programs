#ifndef SIMPLELEXER_H
#define SIMPLELEXER_H

#include <iostream>
#include <cstdlib>
#include <string>
#include <sstream>
using namespace std;

// For some reason, I can't use a forward list in the parser's union unless the lexer knows what it is too.
#include <forward_list>

int countchar (string s, char a);
string stringify (int a);

#endif
